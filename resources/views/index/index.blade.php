<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="shortcut icon" href="/Wedding Dress_files/1f6a23397e.png">
    <meta name="keywords" content="{{session(FRONTSITE)->title}}">
    <meta name="description" content="{{!empty(session(FRONTSITE)->sub_title) ? session(FRONTSITE)->sub_title : session(FRONTSITE)->title}}">
    <title>{{session(FRONTSITE)->title}}</title>
    <link href="/Wedding Dress_files/global.css" rel="stylesheet" type="text/css">
    <link href="/Wedding Dress_files/global(1).css" rel="stylesheet" type="text/css">
    <link href="/Wedding Dress_files/user.css" rel="stylesheet" type="text/css">
    <link href="/Wedding Dress_files/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/Wedding Dress_files/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/Wedding Dress_files/en.js"></script>
    <script type="text/javascript" src="/Wedding Dress_files/global.js"></script>
    <script type="text/javascript" src="/Wedding Dress_files/global.js(1)"></script>
    <script type="text/javascript" src="/Wedding Dress_files/user.js"></script>
    <script type="text/javascript" src="/Wedding Dress_files/main.js"></script>
    <link href="/Wedding Dress_files/font.css" rel="stylesheet" type="text/css">    <!-- Facebook Pixel Code -->
    <!-- End Facebook Pixel Code -->
    <link href="/Wedding Dress_files/index.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="paypal" data-requiremodule="login"
            src="/Wedding Dress_files/login.js"></script>
    <style type="text/css">/*!reset via github.com/premasagar/cleanslate*/
        .LIwPP,
        .LIwPP b,
        .LIwPP img,
        .LIwPP svg {
            -webkit-box-sizing: content-box !important;
            -moz-box-sizing: content-box !important;
            box-sizing: content-box !important;
            background-attachment: scroll !important;
            background-color: transparent !important;
            background-image: none !important;
            background-position: 0 0 !important;
            background-repeat: repeat !important;
            border-color: black !important;
            border-color: currentColor !important;
            border-radius: 0 !important;
            border-style: none !important;
            border-width: medium !important;
            direction: inherit !important;
            display: inline !important;
            float: none !important;
            font-size: inherit !important;
            height: auto !important;
            letter-spacing: normal !important;
            line-height: inherit !important;
            margin: 0 !important;
            max-height: none !important;
            max-width: none !important;
            min-height: 0 !important;
            min-width: 0 !important;
            opacity: 1 !important;
            outline: invert none medium !important;
            overflow: visible !important;
            padding: 0 !important;
            position: static !important;
            text-align: inherit !important;
            text-decoration: inherit !important;
            text-indent: 0 !important;
            text-shadow: none !important;
            text-transform: none !important;
            unicode-bidi: normal !important;
            vertical-align: baseline !important;
            visibility: inherit !important;
            white-space: normal !important;
            width: auto !important;
            word-spacing: normal !important;
        }

        .LIwPP *[dir=rtl] {
            direction: rtl !important;
        }

        .LIwPP {
            direction: ltr !important;
            font-style: normal !important;
            text-align: left !important;
            text-decoration: none !important;
        }

        .LIwPP {
            background-color: #bfbfbf !important;
            background-image: -webkit-linear-gradient(#e0e0e0 20%, #bfbfbf) !important;
            background-image: linear-gradient(#e0e0e0 20%, #bfbfbf) !important;
            background-repeat: no-repeat !important;
            border: 1px solid #bbb !important;
            border-color: #cbcbcb #b2b2b2 #8b8b8b !important;
            -webkit-box-shadow: inset 0 1px #ececec !important;
            box-shadow: inset 0 1px #ececec !important;
            -webkit-box-sizing: border-box !important;
            -moz-box-sizing: border-box !important;
            box-sizing: border-box !important;
            border-radius: 5px !important;
            font-size: 16px !important;
            height: 2em !important;
            margin: 0 !important;
            overflow: hidden !important;
            padding: 0 !important;
            position: relative !important;
            text-align: center !important;
            width: auto !important;
            white-space: nowrap !important;
        }

        .LIwPP,
        .LIwPP b,
        .LIwPP i {
            cursor: pointer !important;
            display: inline-block !important;
            -webkit-user-select: none !important;
            -moz-user-select: none !important;
            -ms-user-select: none !important;
            user-select: none !important;
        }

        .LIwPP b {
            border-left: 1px solid #b2b2b2 !important;
            -webkit-box-shadow: inset 1px 0 rgba(255, 255, 255, .5) !important;
            box-shadow: inset 1px 0 rgba(255, 255, 255, .5) !important;
            color: #333333 !important;
            font: normal 700 0.6875em/1.5 "Helvetica Neue", Arial, sans-serif !important;
            height: 2.25em !important;
            padding: .625em .66667em 0 !important;
            text-shadow: 0 1px 0 #eee !important;
            vertical-align: baseline !important;
        }

        .LIwPP .PPTM,
        .LIwPP i {
            height: 100% !important;
            margin: 0 .4em 0 .5em !important;
            vertical-align: middle !important;
            width: 1em !important;
        }

        .LIwPP i {
            background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAhCAMAAADnC9tbAAAA81BMVEX///////+ozeUAecEARXyozeUARXwAecH///8AecGozeX///8ARXz///+ozeUAecEARXzg7fYAbrPx9/v///8AecFGntKozeUAZqgARXwARXy11OmFv+EAecEAaav///8AecGozeUARXwAW5rZ6fTg7fb5+/3V5/LA2+z///8ARXyozeW92euy0+j5+/0AecEAc7kAbLAASIDR5fH///+ozeUAY6MAT4oARXwAecEARXz///+ozeX///8ARXwAecGozeUARXyozeX///8AecH///8AecEARXyozeWozeX///8ARXwAecGozeUAecH///8ARXwU3q5BAAAATXRSTlMAERERESIiIiIzMzMzRERERERERFVVVVVVVWZmZmZmZnd3d3d3d3d3d4iIiIiIiIiIiIiImZmZmZmqqqqqu7u7u8zMzMzd3d3d7u7u7nVO37gAAAEZSURBVHheTY5rX4IwGEf/aFhWoJV2sRugXcDu2QUMDA0QbdPv/2naw9B13jzb2X5nA1CbLyVxBwWd5RoPhLdU1EjEQjiAQ+K9DWysTmh2f4CmmBnKG89cJrLmySftI1+ISCXnx9wH1D7Y/+UN7JLIoih4uRhxfi5bsTUapZxzvw4goA/y1LKsRhVERq/zNkpk84lXlXC8j26aQoG6qD3iP5uHZwaxg5KtRcnM1UBcLtYMQQzoMJwUZo9EIkQLMEi82oBGC62cvSnQEjMB4JK4Y3KRuG7RGHznQKgemZyyN2ChvnHPcl3Gw9B9uOpPWb4tE8MvRkztCoChENdsfGSaOgpmQtwwE2uo1mcVJYyD3m0+hgJ6zpitxB/jvlwEGmtLjgAAAABJRU5ErkJggg==') no-repeat !important;
            height: 1em !important;
        }

        .LIwPP img.PPTM {
            height: auto !important;
        }

        .LIwPP:hover,
        .LIwPP:active {
            background-color: #a5a5a5 !important;
            background-image: -webkit-linear-gradient(#e0e0e0 20%, #a5a5a5) !important;
            background-image: linear-gradient(#e0e0e0 20%, #a5a5a5) !important;
            background-repeat: no-repeat !important;
        }

        .LIwPP:active {
            border-color: #8c8c8c #878787 #808080 !important;
            -webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.2) !important;
            box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.2) !important;
            outline: 0 !important;
        }

        .LIwPP:hover b {
            -webkit-box-shadow: inset 1px 0 rgba(255, 255, 255, .3) !important;
            box-shadow: inset 1px 0 rgba(255, 255, 255, .3) !important;
        }

        .PPBlue {
            background-color: #0079c1 !important;
            background-image: -webkit-linear-gradient(#00a1ff 20%, #0079c1) !important;
            background-image: linear-gradient(#00a1ff 20%, #0079c1) !important;
            background-repeat: no-repeat !important;
            border-color: #0079c1 #00588b #004b77 !important;
            -webkit-box-shadow: inset 0 1px #4dbeff !important;
            box-shadow: inset 0 1px #4dbeff !important;
        }

        .PPBlue b {
            -webkit-box-shadow: inset 1px 0 rgba(77, 190, 255, .5) !important;
            box-shadow: inset 1px 0 rgba(77, 190, 255, .5) !important;
            border-left-color: #00588b !important;
            color: #f9fcff !important;
            -webkit-font-smoothing: antialiased !important;
            font-smoothing: antialiased !important;
            text-shadow: 0 -1px 0 #00629c !important;
        }

        .PPBlue i {
            background-position: 0 100% !important;
        }

        .PPBlue .PPTM-btm {
            fill: #a8cde5 !important;
        }

        .PPBlue .PPTM-top {
            fill: #fff !important;
        }

        .PPBlue:hover,
        .PPBlue:active {
            background-color: #005282 !important;
            background-image: -webkit-linear-gradient(#0083cf 20%, #005282) !important;
            background-image: linear-gradient(#0083cf 20%, #005282) !important;
            background-repeat: no-repeat !important;
            border-color: #006699 #004466 #003355 !important;
        }

        .PPBlue:hover b,
        .PPBlue:active b {
            text-shadow: 0 -1px 0 #004466 !important;
        }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-35990925-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-35990925-2');
    </script>
</head>

<body class="lang_en w_1200" style="">
<link type="text/css" rel="stylesheet" href="/Wedding Dress_files/open.css">
<style type="text/css">
    .FontColor, a.FontColor, a.FontColor:hover, a:hover {
        color: #EA5765;
    }

    .FontBgColor {
        background-color: #EA5765;
    }

    .FontBorderColor {
        border-color: #EA5765;
    }

    .FontBorderHoverColor:hover, a.FontBorderHoverColor:hover {
        border-color: #EA5765;
    }

    .FontBgHoverColor:hover {
        background-color: #EA5765 !important;
    }

    .FontHoverColor:hover {
        color: #EA5765 !important;
    }

    .NavBgColor {
        background-color: #FFFFFF;
    }

    .NavHoverBgColor:hover {
        background-color: #000000;
    }

    .NavBorderColor1 {
        border-color: #030303;
    }

    .NavBorderColor2 {
        border-color: #0F0F0F;
    }

    .PriceColor {
        color: #1C1C1C;
    }

    .AddtoCartBgColor {
        background-color: #D15666;
    }

    .AddtoCartBorderColor {
        border-color: #D15666;
    }

    .BuyNowBgColor {
        background-color: #EA215C;
    }

    .ReviewBgColor {
        background-color: #EA215C;
    }

    .DiscountBgColor {
        background-color: #EA215C;
    }

    .DiscountBorderColor {
        border-color: #EA215C;
    }

    .ProListBgColor {
        background-color: #B05F72;
    }

    .ProListHoverBgColor:hover {
        background-color: #BE7886;
    }

    .GoodBorderColor {
        border-color: #B01313;
    }

    .GoodBorderHoverColor:hover {
        border-color: #BE2E37;
    }

    .GoodBorderColor.selected {
        border-color: #BE2E37;
    }

    .GoodBorderBottomHoverColor {
        border-bottom-color: #BE2E37;
    }
</style>
<script type="text/javascript">
//    $(window).resize(function () {
//        $(window).webDisplay(0);
//    });
//    $(window).webDisplay(0);
//    var ueeshop_config = {
//        "domain": "http://t069.shop.ueeshop.com",
//        "date": "2017/11/28 13:55:41",
//        "lang": "en",
//        "currency": "USD",
//        "currency_symbols": "$",
//        "currency_rate": "1.0000",
//        "FbAppId": "1594883030731762",
//        "FbPixelOpen": "1",
//        "UserId": "0",
//        "TouristsShopping": "1",
//        "PaypalExcheckout": ""
//    }
</script>

<div id="header" class="wide">
    <div class="container">
        <div class="left_bar fl ">
            <ul class="crossn">
                <li class="block fl">
                    <div class="fl"><strong>Currency:</strong></div>
                    <dl class="fl ">
                        <dt><strong id="currency" class="FontColor">USD</strong></dt>
                        <dd class="currency">
                            <a rel="nofollow" href="javascript:;" data="USD"><img
                                        src="/Wedding Dress_files/7eed9f2e47.jpg" alt="USD">USD</a>
                            <a rel="nofollow" href="javascript:;" data="EUR"><img
                                        src="/Wedding Dress_files/a87887be78.jpg" alt="EUR">EUR</a>
                            <a rel="nofollow" href="javascript:;" data="GBP"><img
                                        src="/Wedding Dress_files/0d196ae885.jpg" alt="GBP">GBP</a>
                            <a rel="nofollow" href="javascript:;" data="CAD"><img
                                        src="/Wedding Dress_files/fc09866559.jpg" alt="CAD">CAD</a>
                            <a rel="nofollow" href="javascript:;" data="AUD"><img
                                        src="/Wedding Dress_files/21d159fb43.jpg" alt="AUD">AUD</a>
                            <a rel="nofollow" href="javascript:;" data="CHF"><img
                                        src="/Wedding Dress_files/ca080529a0.jpg" alt="CHF">CHF</a>
                            <a rel="nofollow" href="javascript:;" data="HKD"><img
                                        src="/Wedding Dress_files/986ff3624d.jpg" alt="HKD">HKD</a>
                            <a rel="nofollow" href="javascript:;" data="JPY"><img
                                        src="/Wedding Dress_files/802c1f1cd6.jpg" alt="JPY">JPY</a>
                            <a rel="nofollow" href="javascript:;" data="RUB"><img
                                        src="/Wedding Dress_files/e374f7aef1.jpg" alt="RUB">RUB</a>
                            <a rel="nofollow" href="javascript:;" data="CNY">CNY</a>
                            <a rel="nofollow" href="javascript:;" data="SAR">SAR</a>
                            <a rel="nofollow" href="javascript:;" data="SGD">SGD</a>
                            <a rel="nofollow" href="javascript:;" data="NZD">NZD</a>
                            <a rel="nofollow" href="javascript:;" data="ARS">ARS</a>
                            <a rel="nofollow" href="javascript:;" data="INR">INR</a>
                            <a rel="nofollow" href="javascript:;" data="COP">COP</a>
                            <a rel="nofollow" href="javascript:;" data="AED">AED</a>
                        </dd>
                    </dl>
                </li>
            </ul>
        </div>
        <div class="right_bar fr">
            <ul>
                <li class="icon">
                    {{--<div class="global_login_sec" style="display: block;">--}}
                        {{--<div class="SignInButton_sec"></div>--}}
                        {{--<div class="signin_box_sec global_signin_module" style="display: none;">--}}
                            {{--<div class="signin_container">--}}
                                {{--<form class="signin_form" name="signin_form" method="post">--}}
                                    {{--<input name="Email" class="g_s_txt" type="text" maxlength="100"--}}
                                           {{--placeholder="Address" format="Email" notnull="">--}}
                                    {{--<div class="blank20"></div>--}}
                                    {{--<input name="Password" class="g_s_txt" type="password" placeholder="Password"--}}
                                           {{--notnull="">--}}
                                    {{--<button class="signin FontBgColor" type="submit">SIGN IN</button>--}}
                                    {{--<input type="hidden" name="do_action" value="user.login">--}}
                                {{--</form>--}}
                                {{--<div id="error_login_box" class="error_login_box"></div>--}}
                                {{--<h4>Sign in with:</h4>--}}
                                {{--<ul>--}}
                                    {{--<script type="text/javascript" src="/Wedding Dress_files/facebook.js"></script>--}}
                                    {{--<li id="fb_button" scope="public_profile, email" onclick="checkLoginState();"--}}
                                        {{--appid="1594883030731762"></li>--}}
                                    {{--<script type="text/javascript" src="/Wedding Dress_files/google.js"></script>--}}
                                    {{--<li id="google_login">--}}
                                        {{--<div id="google_btn"--}}
                                             {{--clientid="640892902807-64ev2hjtu0141m7lsn9k15mfm9b5nak5.apps.googleusercontent.com">--}}
                                            {{--&nbsp;--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--<script type="text/javascript" src="/Wedding Dress_files/api.js"></script>--}}
                                    {{--<li id="paypalLogin"--}}
                                        {{--appid="AXaj3xAfsUrUujWbtErMA7ElMc6gEeou8TMJMibnisJR21oFmbZ-4eCrvx5d"--}}
                                        {{--u="http://t011.uee.ly200.net/?do_action=user.user_oauth&amp;Type=Paypal">--}}
                                        {{--<button id="LIwPP2829892" class="LIwPP PPBlue">--}}
                                            {{--<svg class="PPTM" xmlns="http://www.w3.org/2000/svg" version="1.1"--}}
                                                 {{--width="16px" height="17px" viewBox="0 0 16 17">--}}
                                                {{--<path class="PPTM-btm" fill="#0079c1"--}}
                                                      {{--d="m15.603 3.917c-0.264-0.505-0.651-0.917-1.155-1.231-0.025-0.016-0.055-0.029-0.081-0.044 0.004 0.007 0.009 0.014 0.013 0.021 0.265 0.506 0.396 1.135 0.396 1.891 0 1.715-0.712 3.097-2.138 4.148-1.425 1.052-3.418 1.574-5.979 1.574h-0.597c-0.45 0-0.9 0.359-1.001 0.798l-0.719 3.106c-0.101 0.438-0.552 0.797-1.002 0.797h-1.404l-0.105 0.457c-0.101 0.438 0.184 0.798 0.633 0.798h2.1c0.45 0 0.9-0.359 1.001-0.798l0.718-3.106c0.101-0.438 0.551-0.797 1.002-0.797h0.597c2.562 0 4.554-0.522 5.979-1.574 1.426-1.052 2.139-2.434 2.139-4.149 0-0.755-0.132-1.385-0.397-1.891z"></path>--}}
                                                {{--<path class="PPTM-top" fill="#00457c"--}}
                                                      {{--d="m9.27 6.283c-0.63 0.46-1.511 0.691-2.641 0.691h-0.521c-0.45 0-0.736-0.359-0.635-0.797l0.628-2.72c0.101-0.438 0.552-0.797 1.002-0.797h0.686c0.802 0 1.408 0.136 1.814 0.409 0.409 0.268 0.611 0.683 0.611 1.244 0 0.852-0.315 1.507-0.944 1.97zm3.369-5.42c-0.913-0.566-2.16-0.863-4.288-0.863h-4.372c-0.449 0-0.9 0.359-1.001 0.797l-2.957 12.813c-0.101 0.439 0.185 0.798 0.634 0.798h2.099c0.45 0 0.901-0.358 1.003-0.797l0.717-3.105c0.101-0.438 0.552-0.797 1.001-0.797h0.598c2.562 0 4.554-0.524 5.979-1.575 1.427-1.051 2.139-2.433 2.139-4.148-0.001-1.365-0.439-2.425-1.552-3.123z"></path>--}}
                                            {{--</svg>--}}
                                            {{--<b>Log In with PayPal</b></button>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                                {{--<div class="blank20"></div>--}}
                                {{--<a href="http://t069.shop.ueeshop.com/account/sign-up.html"--}}
                                   {{--class="signup FontBorderColor FontColor">JOIN FREE</a>--}}
                                {{--<a href="http://t069.shop.ueeshop.com/account/forgot.html" class="forgot">Forgot your--}}
                                    {{--password?</a>--}}
                                {{--<div class="clear"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<script type="text/javascript">--}}
                        {{--$(document).ready(function () {--}}
                            {{--user_obj.sign_in_init();--}}
                            {{--var timer;--}}
                            {{--$('.global_login_sec').hover(--}}
                                {{--function () {--}}
                                    {{--clearTimeout(timer);--}}
                                    {{--$(this).find('.SignInButton_sec').addClass('cur');--}}
                                    {{--$(this).find('.signin_box_sec').fadeIn();--}}
                                {{--},--}}
                                {{--function () {--}}
                                    {{--var _this = $(this);--}}
                                    {{--timer = setTimeout(function () {--}}
                                        {{--_this.find('.SignInButton_sec').removeClass('cur');--}}
                                        {{--_this.find('.signin_box_sec').fadeOut();--}}
                                    {{--}, 500);--}}
                                {{--}--}}
                            {{--);--}}
                        {{--});--}}
                    {{--</script>--}}
                </li>
                {{--<li class="icon">--}}
                    {{--<div class="header_cart" lang="_en"><a href="http://t069.shop.ueeshop.com/cart/" class="cart_inner"><span--}}
                                    {{--class="cart_count FontBgColor">0</span></a></div>--}}
                {{--</li>--}}
                <li class="icon icon_re">
                    <div class="search"><a href="javascript:;"></a></div>
                    <div class="search_form">
                        <form action="/search/" method="get" class="form">
                            <input type="text" class="text fl" placeholder="" name="Keyword" notnull="" value="">
                            <input type="submit" class="button fr" value="">
                            <div class="clear"></div>
                            <i></i>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        {{--<div class="logo"><h1><a href="/"><img src="/Wedding Dress_files/3284a8ba19.png"--}}
                                                                           {{--alt="Trendy Furniture Store"></a></h1></div>--}}
        <div class="logo"><h1><a href="/" style="color:black;font-size:40px;text-decoration:none">{{session(FRONTSITE)->title}}</a></h1></div>
        <div class="clear"></div>
    </div>
</div>
<div id="nav_outer">
    <div class="wide">
        <div id="nav" class="container" style="overflow: visible;">
            <ul class="nav_item">
                <li>
                    <a href="/">Home</a>
                </li>
                {{--<li>--}}
                    {{--<a class="FontColor" href="http://t069.shop.ueeshop.com/products/">Products</a>--}}
                    {{--<div class="nav_sec">--}}
                        {{--<div class="nav_sec_box">--}}
                            {{--<div class="wide">--}}
                                {{--<div class="container">--}}
                                    {{--<div class="nav_sec_list">--}}
                                        {{--<a class="nav_sec_a" href="http://t069.shop.ueeshop.com/c/wedding-dresses_0419"--}}
                                           {{--title="Wedding Dresses">Wedding Dresses</a>--}}
                                        {{--<div class="nav_thd">--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/luxury-wedding-dresses_0423"--}}
                                                                         {{--title="Luxury Wedding Dresses">Luxury Wedding--}}
                                                    {{--Dresses</a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="nav_sec_list">--}}
                                        {{--<a class="nav_sec_a"--}}
                                           {{--href="http://t069.shop.ueeshop.com/c/special-occasion-dresses_0429"--}}
                                           {{--title="Special  Occasion  Dresses">Special Occasion Dresses</a>--}}
                                        {{--<div class="nav_thd">--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/prom-dresses_0430"--}}
                                                                         {{--title="Prom Dresses">Prom Dresses</a></div>--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/evening-dresses_0431"--}}
                                                                         {{--title="Evening Dresses">Evening Dresses</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="nav_sec_list">--}}
                                        {{--<a class="nav_sec_a" href="http://t069.shop.ueeshop.com/c/wedding-apparel_0434"--}}
                                           {{--title="Wedding Apparel">Wedding Apparel</a>--}}
                                        {{--<div class="nav_thd">--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/wedding-party-dresses_0435"--}}
                                                                         {{--title="Wedding Party Dresses">Wedding Party--}}
                                                    {{--Dresses</a></div>--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/junior-bridesmaid-dresses_0436"--}}
                                                                         {{--title="Junior Bridesmaid Dresses">Junior--}}
                                                    {{--Bridesmaid Dresses</a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="nav_sec_list">--}}
                                        {{--<a class="nav_sec_a" href="http://t069.shop.ueeshop.com/c/new-wedding_0444"--}}
                                           {{--title="New Wedding">New Wedding</a>--}}
                                        {{--<div class="nav_thd">--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/ball-gown-dresses_0445"--}}
                                                                         {{--title="Ball Gown Dresses">Ball Gown Dresses</a>--}}
                                            {{--</div>--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/popular-prom-dresses_0446"--}}
                                                                         {{--title="Popular Prom Dresses">Popular Prom--}}
                                                    {{--Dresses</a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="clear"></div>--}}
                                    {{--<div class="clear"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/GroupBuying.html">Group Purchase</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/holiday.html">Holiday</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/FlashSale.html">Flash Sale</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/blog/">Blog</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/c/special-occasion-dresses_0429">Special Occasion Dresses</a>--}}
                {{--</li>--}}
                {{--<li style="display: none;">--}}
                    {{--<a href="http://t069.shop.ueeshop.com/c/wedding-dresses_0419">Wedding Dresses</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</div>
{{--<div id="banner">--}}
    {{--<div style="overflow:hidden;">--}}
        {{--<script type="text/javascript" src="/Wedding Dress_files/jQuery.blockUI.js"></script>--}}
        {{--<script type="text/javascript" src="/Wedding Dress_files/jquery.SuperSlide.js"></script>--}}
        {{--<style type="text/css">--}}
            {{--.slideBox_1 {--}}
                {{--overflow: hidden;--}}
                {{--position: relative;--}}
            {{--}--}}

            {{--.slideBox_1 .hd {--}}
                {{--height: 15px;--}}
                {{--overflow: hidden;--}}
                {{--position: absolute;--}}
                {{--bottom: 15px;--}}
                {{--z-index: 1;--}}
            {{--}--}}

            {{--.slideBox_1 .hd ul {--}}
                {{--overflow: hidden;--}}
                {{--zoom: 1;--}}
                {{--float: left;--}}
            {{--}--}}

            {{--.slideBox_1 .hd ul li {--}}
                {{--float: left;--}}
                {{--margin-left: 5px;--}}
                {{--width: 10px;--}}
                {{--height: 10px;--}}
                {{---webkit-border-radius: 5px;--}}
                {{---moz-border-radius: 5px;--}}
                {{--border-radius: 5px;--}}
                {{--background: #f1f1f1;--}}
                {{--cursor: pointer;--}}
            {{--}--}}

            {{--.slideBox_1 .hd ul li:first-child {--}}
                {{--margin-left: 0;--}}
            {{--}--}}

            {{--.slideBox_1 .hd ul li.on {--}}
                {{--background: #f00;--}}
                {{--color: #fff;--}}
            {{--}--}}

            {{--.slideBox_1 .bd {--}}
                {{--position: relative;--}}
                {{--height: 100%;--}}
                {{--z-index: 0;--}}
            {{--}--}}

            {{--.slideBox_1 .bd ul li a {--}}
                {{--display: block;--}}
                {{--background-position: center top;--}}
                {{--background-repeat: no-repeat;--}}
            {{--}--}}
        {{--</style>--}}
        {{--<div id="slideBox_1" class="slideBox_1">--}}
            {{--<div class="hd" style="left: 946.5px;">--}}
                {{--<ul>--}}
                    {{--<li class="on"></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="bd" style="width: 1903px;">--}}
                {{--<ul style="width: 1903px;">--}}
                    {{--<li style="width: 1903px;"><a href="javascript:;"><img src="/Wedding Dress_files/34b7ce5d94.jpg"--}}
                                                                           {{--alt="" style="width: 1903px;"></a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<script type="text/javascript">jQuery(document).ready(function () {--}}
                {{--jQuery(".slideBox_1").slide({mainCell: ".bd ul", effect: "fade", autoPlay: true, interTime: 5000});--}}
            {{--});</script>--}}
    {{--</div>--}}
{{--</div>--}}
<style>
    .fl{margin:10px 12px;}
</style>
<div class="blank30"></div>
<div class="intt wide">
    <div class="container">
        {{--@foreach ($activity_list as $k=>$row)--}}
            {{--<div class="fl">--}}
                {{--<a href="{{url("activity/{$row->id}")}}"><div><img src="{{$row->image_url}}" alt=""></div></a>--}}
            {{--</div>--}}
            {{--@if (($k+1)%3==0)--}}
                {{--<div class="clear"></div>--}}
            {{--@endif--}}
        {{--@endforeach--}}

        @foreach ($activity_list as $k=>$row)

            {{--@if (!empty($row->creator))--}}
                {{--{{$row->creator->name}}--}}
            {{--@endif--}}

            @if (!empty($row->image_url))
                <div class="fl0 weiyi">
                    <a href="{{url("activity/".urlencode($row->name))}}" title="{{$row->name}}">
                        <div class="picgroup"><img src="{{$row->image_url}}" alt="{{$row->name}}">
                        </div>
                    </a>
                </div>
            @elseif ($row->item_count  > 4)
                <div class="fl1 weiyi1">
                    <a href="{{url("activity/".urlencode($row->name))}}" title="{{$row->name}}">
                    @foreach ($row->item_list->take(9) as $j=>$item)
                        <div class="picgroup"><img src="{{$item->image_url}}" alt="{{$item->title}}">
                        </div>
                    @endforeach
                    </a>
                </div>
            @elseif ($row->item_count  > 1)
                <div class="fl2 weiyi1">
                    <a href="{{url("activity/".urlencode($row->name))}}" title="{{$row->name}}">
                    @foreach ($row->item_list->take(9) as $j=>$item)
                        <div class="picgroup"><img src="{{$item->image_url}}" alt="{{$item->title}}">
                        </div>
                    @endforeach
                    </a>
                </div>
            @endif

            @if (($k+1)%4==0)
            <div class="clear"></div>
            @endif

        @endforeach
        
    </div>
</div>
{{--<div class="blank30"></div>--}}
{{--<div class="intt wide">--}}
    {{--<div class="container">--}}
        {{--<div class="fl">--}}
            {{--<div><img src="/Wedding Dress_files/5dfc08f8af.png" alt=""></div>--}}
        {{--</div>--}}
        {{--<div class="fr">--}}
            {{--<div><img src="/Wedding Dress_files/dbd7dadd58.png" alt=""></div>--}}
        {{--</div>--}}
        {{--<div class="clear"></div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="blank30"></div>--}}
{{--<div class="intb wide">--}}
    {{--<div class="container">--}}
        {{--<div class="fl">--}}
            {{--<div class="img">--}}
                {{--<div><img src="/Wedding Dress_files/c4ec36dc4d.png"--}}
                          {{--alt="Echocardiography action The perfect wedding dress"></div>--}}
            {{--</div>--}}
            {{--<div class="con">--}}
                {{--<div class="title">Echocardiography action The perfect wedding dress</div>--}}
                {{--<div class="intro"></div>--}}
                {{--<div class="btn"><a class="FontBgHoverColor"--}}
                                    {{--href="/">BUY NOW</a></div>--}}
            {{--</div>--}}
            {{--<div class="clear"></div>--}}
        {{--</div>--}}
        {{--<div class="fr">--}}
            {{--<div><img src="/Wedding Dress_files/7855b02ed2.png" alt=""></div>--}}
        {{--</div>--}}
        {{--<div class="clear"></div>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="blank30"></div>
<div class="blank15"></div>
<div class="inmt wide">
    <div class="container">
        <div class="g_title">{{$list_title}}</div>
        <div class="g_sign" style="height:auto;">{!! !empty($list_description) ? $list_description : '' !!}</div>
        <div class="category">
        </div>
        <div class="blank30"></div>
        <div class="blank12"></div>
        <div class="box">
            @foreach ($list as $k=>$row)
                <div class="prod_box {{$k%4==0 ? 'prod_box_nor' : ''}}">
                    <div class="img v_m">
                        <a class="pic_box"
                           href="/product/{{$row->asin}}/{{$row->seo_title}}"><img
                                    src="{{$row->image_url}}"
                                    alt="{{$row->title}}"><span></span></a>
                        {{--<em class="icon_discount DiscountBgColor"><b>99</b>%<br>OFF</em><em--}}
                                {{--class="icon_discount_foot DiscountBorderColor"></em> <em--}}
                                {{--class="icon_seckill DiscountBgColor">Sale</em>--}}
                        {{--<div class="mask">--}}
                            {{--<a class="see FontBgHoverColor"--}}
                               {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0984.html"></a>--}}
                            {{--<a class="favo add_favorite FontBgHoverColor" data="984" href="javascript:;"></a>--}}
                            {{--<a class="cart FontBgHoverColor"--}}
                               {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0984.html"></a>--}}
                        {{--</div>--}}
                    </div>
                    <div class="name"><a target="_blank"
                                href="{{$row->url}}"
                                title="{{$row->title}}">{{$row->title}}</a></div>
                    <div class="rating">
                        <img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img
                                src="/Wedding Dress_files/rating_cur.jpg"><img
                                src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">
                    </div>
                    <div class="price">
                        <em class="currency_data FontColor">$</em>
                        <span class="price_data FontColor" data="5.00" keyid="984">{{substr($row->price_text,1)}}</span>
                    </div>
                    <div class="name">
                        @foreach ($row->keywords as $keyword)
                        <a target="_blank" href="{{url("tag/".urlencode($keyword->word))}}" title="{{$keyword->word}}">#{{$keyword->word}}</a>
                        @endforeach
                    </div>
                </div>
            @endforeach
            {{--<div class="prod_box prod_box_nor">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0984.html"><img--}}
                                {{--src="/Wedding Dress_files/a424891312.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style Straps Lace Mother of the Bride Dresses #GZ015"><span></span></a>--}}
                    {{--<em class="icon_discount DiscountBgColor"><b>99</b>%<br>OFF</em><em--}}
                            {{--class="icon_discount_foot DiscountBorderColor"></em> <em--}}
                            {{--class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0984.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="984" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0984.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0984.html"--}}
                            {{--title="2015 Style Straps Lace Mother of the Bride Dresses #GZ015">2015 Style Straps Lace--}}
                        {{--Mother of the Bride Dresses #GZ015</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor"--}}
                                                                                   {{--data="5.00" keyid="984">5.00</span>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0983.html"><img--}}
                                {{--src="/Wedding Dress_files/b57328a827.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style A-line Straps Lace Mother of the Bride Dresses #GZ015"><span></span></a>--}}
                    {{--<em class="icon_discount DiscountBgColor"><b>99</b>%<br>OFF</em><em--}}
                            {{--class="icon_discount_foot DiscountBorderColor"></em> <em--}}
                            {{--class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0983.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="983" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0983.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0983.html"--}}
                            {{--title="2015 Style A-line Straps Lace Mother of the Bride Dresses #GZ015">2015 Style A-line--}}
                        {{--Straps Lace Mother of the Bride Dresses #GZ015</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor"--}}
                                                                                   {{--data="5.00" keyid="983">5.00</span>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0982.html"><img--}}
                                {{--src="/Wedding Dress_files/b3bd1bc7ec.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style A-line Straps Lace Mother of the Bride Dresses #GZ015"><span></span></a>--}}
                    {{--<em class="icon_discount DiscountBgColor"><b>37</b>%<br>OFF</em><em--}}
                            {{--class="icon_discount_foot DiscountBorderColor"></em> <em--}}
                            {{--class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0982.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="982" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0982.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0982.html"--}}
                            {{--title="2015 Style A-line Straps Lace Mother of the Bride Dresses #GZ015">2015 Style A-line--}}
                        {{--Straps Lace Mother of the Bride Dresses #GZ015</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor"--}}
                                                                                   {{--data="347.00"--}}
                                                                                   {{--keyid="982">347.00</span></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0981.html"><img--}}
                                {{--src="/Wedding Dress_files/1f67660cd4.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style A-line Straps Lace Mother of the Bride Dresses #GZ015"><span></span></a>--}}
                    {{--<em class="icon_discount DiscountBgColor"><b>37</b>%<br>OFF</em><em--}}
                            {{--class="icon_discount_foot DiscountBorderColor"></em> <em--}}
                            {{--class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0981.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="981" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0981.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0981.html"--}}
                            {{--title="2015 Style A-line Straps Lace Mother of the Bride Dresses #GZ015">2015 Style A-line--}}
                        {{--Straps Lace Mother of the Bride Dresses #GZ015</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor"--}}
                                                                                   {{--data="347.00"--}}
                                                                                   {{--keyid="981">347.00</span></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box prod_box_nor">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0980.html"><img--}}
                                {{--src="/Wedding Dress_files/73076f6f99.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style Straps Lace Mother of the Bride Dresses #GZ015"><span></span></a>--}}
                    {{--<em class="icon_discount DiscountBgColor"><b>99</b>%<br>OFF</em><em--}}
                            {{--class="icon_discount_foot DiscountBorderColor"></em> <em--}}
                            {{--class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0980.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="980" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0980.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0980.html"--}}
                            {{--title="2015 Style Straps Lace Mother of the Bride Dresses #GZ015">2015 Style Straps Lace--}}
                        {{--Mother of the Bride Dresses #GZ015</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor"--}}
                                                                                   {{--data="5.00" keyid="980">5.00</span>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-sweetheart-appliques-evening-dresses-gz065_p0972.html"><img--}}
                                {{--src="/Wedding Dress_files/f0bdbcb626.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style Sheath/Column Sweetheart Appliques Evening Dresses #GZ065"><span></span></a>--}}
                    {{--<em class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-sweetheart-appliques-evening-dresses-gz065_p0972.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="972" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-sweetheart-appliques-evening-dresses-gz065_p0972.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-sweetheart-appliques-evening-dresses-gz065_p0972.html"--}}
                            {{--title="2015 Style Sheath/Column Sweetheart Appliques Evening Dresses #GZ065">2015 Style--}}
                        {{--Sheath/Column Sweetheart Appliques Evening Dresses #GZ065</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor" data="5"--}}
                                                                                   {{--keyid="972">5.00</span></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-v-neck-lace-evening-dresses-gz020_p0971.html"><img--}}
                                {{--src="/Wedding Dress_files/d215c7c564.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style Sheath/Column V-neck Lace Evening Dresses #GZ020"><span></span></a>--}}
                    {{--<em class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-v-neck-lace-evening-dresses-gz020_p0971.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="971" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-v-neck-lace-evening-dresses-gz020_p0971.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-v-neck-lace-evening-dresses-gz020_p0971.html"--}}
                            {{--title="2015 Style Sheath/Column V-neck Lace Evening Dresses #GZ020">2015 Style Sheath/Column--}}
                        {{--V-neck Lace Evening Dresses #GZ020</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor" data="5"--}}
                                                                                   {{--keyid="971">5.00</span></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-scoop-lace-mother-of-the-bride-dresses-gz013_p0969.html"><img--}}
                                {{--src="/Wedding Dress_files/559654668f.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style Sheath/Column Scoop Lace Mother of the Bride Dresses #GZ013"><span></span></a>--}}
                    {{--<em class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-scoop-lace-mother-of-the-bride-dresses-gz013_p0969.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="969" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-scoop-lace-mother-of-the-bride-dresses-gz013_p0969.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-scoop-lace-mother-of-the-bride-dresses-gz013_p0969.html"--}}
                            {{--title="2015 Style Sheath/Column Scoop Lace Mother of the Bride Dresses #GZ013">2015 Style--}}
                        {{--Sheath/Column Scoop Lace Mother of the Bride Dresses #GZ013</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor" data="4"--}}
                                                                                   {{--keyid="969">4.00</span></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box prod_box_nor">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-off-the-shoulder-rhinestone-prom-dresses-evening-dresses-gy453_p0968.html"><img--}}
                                {{--src="/Wedding Dress_files/f07f501394.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style Sheath/Column Off-the-shoulder Rhinestone Prom Dresses/Evening Dresses #GY453"><span></span></a>--}}
                    {{--<em class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-off-the-shoulder-rhinestone-prom-dresses-evening-dresses-gy453_p0968.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="968" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-off-the-shoulder-rhinestone-prom-dresses-evening-dresses-gy453_p0968.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-off-the-shoulder-rhinestone-prom-dresses-evening-dresses-gy453_p0968.html"--}}
                            {{--title="2015 Style Sheath/Column Off-the-shoulder Rhinestone Prom Dresses/Evening Dresses #GY453">2015--}}
                        {{--Style Sheath/Column Off-the-shoulder Rhinestone Prom Dresses/Evening Dresses #GY453</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor" data="4"--}}
                                                                                   {{--keyid="968">4.00</span></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-bateau-rhinestone-prom-dresses-evening-dresses-gy450_p0967.html"><img--}}
                                {{--src="/Wedding Dress_files/f1b73f9966.gif.500x500.gif"--}}
                                {{--alt="2015 Style Sheath/Column Bateau Rhinestone Prom Dresses/Evening Dresses #GY450"><span></span></a>--}}
                    {{--<em class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-bateau-rhinestone-prom-dresses-evening-dresses-gy450_p0967.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="967" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-bateau-rhinestone-prom-dresses-evening-dresses-gy450_p0967.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-bateau-rhinestone-prom-dresses-evening-dresses-gy450_p0967.html"--}}
                            {{--title="2015 Style Sheath/Column Bateau Rhinestone Prom Dresses/Evening Dresses #GY450">2015--}}
                        {{--Style Sheath/Column Bateau Rhinestone Prom Dresses/Evening Dresses #GY450</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor" data="5"--}}
                                                                                   {{--keyid="967">5.00</span></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-v-neck-floor-length-chiffon-mother-of-the-bride-dresses-qm069_p0957.html"><img--}}
                                {{--src="/Wedding Dress_files/a3287d3cad.jpg.500x500.jpg"--}}
                                {{--alt="2015 Style Sheath/Column V-neck Floor-length Chiffon Mother of the Bride Dresses #QM069"><span></span></a>--}}
                    {{--<em class="icon_seckill DiscountBgColor" style="display: inline;">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-v-neck-floor-length-chiffon-mother-of-the-bride-dresses-qm069_p0957.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="957" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-v-neck-floor-length-chiffon-mother-of-the-bride-dresses-qm069_p0957.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/2015-style-sheath-column-v-neck-floor-length-chiffon-mother-of-the-bride-dresses-qm069_p0957.html"--}}
                            {{--title="2015 Style Sheath/Column V-neck Floor-length Chiffon Mother of the Bride Dresses #QM069">2015--}}
                        {{--Style Sheath/Column V-neck Floor-length Chiffon Mother of the Bride Dresses #QM069</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor" data="5"--}}
                                                                                   {{--keyid="957">152.00</span></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/sheath-column-scoop-rhinestone-prom-dresses-evening-dresses-gx453_p0948.html"><img--}}
                                {{--src="/Wedding Dress_files/c2ab46f3f3.jpg.500x500.jpg"--}}
                                {{--alt="Sheath/Column Scoop Rhinestone Prom Dresses/Evening Dresses #GX453"><span></span></a>--}}
                    {{--<em class="icon_seckill DiscountBgColor">Sale</em>--}}
                    {{--<div class="mask">--}}
                        {{--<a class="see FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/sheath-column-scoop-rhinestone-prom-dresses-evening-dresses-gx453_p0948.html"></a>--}}
                        {{--<a class="favo add_favorite FontBgHoverColor" data="948" href="javascript:;"></a>--}}
                        {{--<a class="cart FontBgHoverColor"--}}
                           {{--href="http://t069.shop.ueeshop.com/sheath-column-scoop-rhinestone-prom-dresses-evening-dresses-gx453_p0948.html"></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="name"><a--}}
                            {{--href="http://t069.shop.ueeshop.com/sheath-column-scoop-rhinestone-prom-dresses-evening-dresses-gx453_p0948.html"--}}
                            {{--title="Sheath/Column Scoop Rhinestone Prom Dresses/Evening Dresses #GX453">Sheath/Column--}}
                        {{--Scoop Rhinestone Prom Dresses/Evening Dresses #GX453</a></div>--}}
                {{--<div class="rating">--}}
                    {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                            {{--src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg">--}}
                {{--</div>--}}
                {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor" data="5"--}}
                                                                                   {{--keyid="948">5.00</span></div>--}}
            {{--</div>--}}
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="blank30"></div>
{{--<div class="inmb wide">--}}
    {{--<div class="container">--}}
        {{--<div class="g_title">Special Offer Products</div>--}}
        {{--<div class="g_sign"></div>--}}
        {{--<div class="blank30"></div>--}}
        {{--<div class="box">--}}
            {{--<div class="prod_box prod_box_nor">--}}
                {{--<div class="img fl v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0992.html"><img--}}
                                {{--src="/Wedding Dress_files/ac0887b352.jpg.240x240.jpg"--}}
                                {{--alt="2015 Style  Straps Lace Mother of the Bride Dresses #GZ015"><span></span></a>--}}
                    {{--<em class="icon_discount DiscountBgColor" style="display: none;"><b>99</b>%<br>OFF</em><em--}}
                            {{--class="icon_discount_foot DiscountBorderColor" style="display: none;"></em> <em--}}
                            {{--class="icon_seckill DiscountBgColor" style="display: inline;">Sale</em>--}}
                {{--</div>--}}
                {{--<div class="con fr">--}}
                    {{--<div class="name"><a--}}
                                {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0992.html"--}}
                                {{--title="2015 Style  Straps Lace Mother of the Bride Dresses #GZ015">2015 Style Straps--}}
                            {{--Lace Mother of the Bride Dresses #GZ015</a></div>--}}
                    {{--<div class="rating">--}}
                        {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"></div>--}}
                    {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor"--}}
                                                                                       {{--data="5.00"--}}
                                                                                       {{--keyid="992">156.00</span></div>--}}
                    {{--<div class="blank12"></div>--}}
                    {{--<div class="btn"><a class="FontBgHoverColor"--}}
                                        {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0992.html">ADD--}}
                            {{--TO CART</a></div>--}}
                {{--</div>--}}
                {{--<div class="clear"></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img fl v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0991.html"><img--}}
                                {{--src="/Wedding Dress_files/0c51f6b241.jpg.240x240.jpg"--}}
                                {{--alt="2015 Style Straps Lace Mother of the Bride Dresses #GZ015"><span></span></a>--}}
                    {{--<em class="icon_discount DiscountBgColor" style="display: none;"><b>37</b>%<br>OFF</em><em--}}
                            {{--class="icon_discount_foot DiscountBorderColor" style="display: none;"></em> <em--}}
                            {{--class="icon_seckill DiscountBgColor" style="display: inline;">Sale</em>--}}
                {{--</div>--}}
                {{--<div class="con fr">--}}
                    {{--<div class="name"><a--}}
                                {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0991.html"--}}
                                {{--title="2015 Style Straps Lace Mother of the Bride Dresses #GZ015">2015 Style Straps Lace--}}
                            {{--Mother of the Bride Dresses #GZ015</a></div>--}}
                    {{--<div class="rating">--}}
                        {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"></div>--}}
                    {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor"--}}
                                                                                       {{--data="347.00"--}}
                                                                                       {{--keyid="991">165.00</span></div>--}}
                    {{--<div class="blank12"></div>--}}
                    {{--<div class="btn"><a class="FontBgHoverColor"--}}
                                        {{--href="http://t069.shop.ueeshop.com/2015-style-straps-lace-mother-of-the-bride-dresses-gz015_p0991.html">ADD--}}
                            {{--TO CART</a></div>--}}
                {{--</div>--}}
                {{--<div class="clear"></div>--}}
            {{--</div>--}}
            {{--<div class="prod_box ">--}}
                {{--<div class="img fl v_m">--}}
                    {{--<a class="pic_box"--}}
                       {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0990.html"><img--}}
                                {{--src="/Wedding Dress_files/1ed6d05146.jpg.240x240.jpg"--}}
                                {{--alt="2015 Style A-line Straps Lace Mother of the Bride Dresses #GZ015"><span></span></a>--}}
                    {{--<em class="icon_discount DiscountBgColor" style="display: none;"><b>99</b>%<br>OFF</em><em--}}
                            {{--class="icon_discount_foot DiscountBorderColor" style="display: none;"></em> <em--}}
                            {{--class="icon_seckill DiscountBgColor" style="display: inline;">Sale</em>--}}
                {{--</div>--}}
                {{--<div class="con fr">--}}
                    {{--<div class="name"><a--}}
                                {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0990.html"--}}
                                {{--title="2015 Style A-line Straps Lace Mother of the Bride Dresses #GZ015">2015 Style--}}
                            {{--A-line Straps Lace Mother of the Bride Dresses #GZ015</a></div>--}}
                    {{--<div class="rating">--}}
                        {{--<img src="/Wedding Dress_files/rating_cur.jpg"><img src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"><img--}}
                                {{--src="/Wedding Dress_files/rating_cur.jpg"></div>--}}
                    {{--<div class="price"><em class="currency_data FontColor">$</em><span class="price_data FontColor"--}}
                                                                                       {{--data="5.00"--}}
                                                                                       {{--keyid="990">125.00</span></div>--}}
                    {{--<div class="blank12"></div>--}}
                    {{--<div class="btn"><a class="FontBgHoverColor"--}}
                                        {{--href="http://t069.shop.ueeshop.com/2015-style-a-line-straps-lace-mother-of-the-bride-dresses-gz015_p0990.html">ADD--}}
                            {{--TO CART</a></div>--}}
                {{--</div>--}}
                {{--<div class="clear"></div>--}}
            {{--</div>--}}
            {{--<div class="clear"></div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="blank30"></div>--}}
{{--<div class="blank30"></div>--}}
{{--<div class="inbt wide">--}}
    {{--<div class="container">--}}
        {{--<div class="link">--}}
            {{--<div class="partners_box"><a href="http://t011.uee.ly200.net/" title="1" target="_blank"><img--}}
                            {{--src="/Wedding Dress_files/2965c4b96c.png" alt="1"></a><a href="http://t011.uee.ly200.net/"--}}
                                                                                      {{--title="2" target="_blank"><img--}}
                            {{--src="/Wedding Dress_files/a770965f78.png" alt="2"></a><a href="http://t011.uee.ly200.net/"--}}
                                                                                      {{--title="3" target="_blank"><img--}}
                            {{--src="/Wedding Dress_files/824387cf69.png" alt="3"></a><a href="http://t011.uee.ly200.net/"--}}
                                                                                      {{--title="4" target="_blank"><img--}}
                            {{--src="/Wedding Dress_files/1f6a23397e.png" alt="4"></a><a href="http://t011.uee.ly200.net/"--}}
                                                                                      {{--title="5" target="_blank"><img--}}
                            {{--src="/Wedding Dress_files/a6d3990d25.png" alt="5"></a></div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="inbb FontBgColor min">
    <div class="wide">
        <div class="container">
            <span class="txt">Sign up from our online store style news</span>
            <span class="input">
                <form id="newsletter_form">
                    <input type="text" class="text" name="Email" value="" notnull="" format="Email">
                    <input type="submit" class="subscribe" value="Submit">
                </form>
            </span>
        </div>
    </div>
</div>
<div id="footer" class="min">
    <div class="wide">
        <div class="container">
            <div class="top">
                <div class="con fl">
                    <div class="logo">
                        {{--<div><img src="/Wedding Dress_files/1901574b0e.png" alt=""></div>--}}
                        <div><h1><a href="/" style="color:white;font-size:30px;text-decoration:none">{{session(FRONTSITE)->title}}</a></h1></div>
                    </div>
                    {{--<div class="share">--}}
                        {{--<div class="follow_toolbox clearfix">--}}
                            {{--<ul>--}}
                                {{--<li><a rel="nofollow" class="follow_facebook" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Facebook">Facebook</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_twitter" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Twitter">Twitter</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_pinterest" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Pinterest">Pinterest</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_youtube" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="YouTube">YouTube</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_google" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Google">Google</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_vk" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="VK">VK</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_linkedin" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="LinkedIn">LinkedIn</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_instagram" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Instagram">Instagram</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="nav fr">
                    <dl>
                        <dt>Collections</dt>
                        @foreach ($collections_list as $row)
                            <dd><a href="{{url("activity/".urlencode($row->name))}}" title="{{$row->name}}">{{$row->name}}</a></dd>
                        @endforeach
                    </dl>
                    {{--@if (strpos($random_key,'1'))--}}
                    {{--<dl>--}}
                        {{--<dt>Overview</dt>--}}
                        {{--@if (strpos($random_key,'3'))--}}
                        {{--<dd><a href="{{url('article/1')}}"--}}
                               {{--title="Location &amp; Working Hours">Location &amp; Working Hours</a></dd>--}}
                        {{--@endif--}}
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/about-rockliffeocom_a0019.html"--}}
                               {{--title="About Rockliffeo.com">About Rockliffeo.com</a></dd>--}}
                        {{--@if (strpos($random_key,'4'))--}}
                        {{--<dd><a href="{{url('article/2')}}" title="Our Guarantee">Our--}}
                                {{--Guarantee</a></dd>--}}
                        {{--@endif--}}
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/location-amp-working-hours_a0007.html"--}}
                               {{--title="Location &amp; Working Hours">Location &amp; Working Hours</a></dd>--}}
                    {{--</dl>--}}
                    {{--@endif--}}
                    {{--@if (strpos($random_key,'2'))--}}
                    {{--<dl>--}}
                        {{--<dt>Products &amp; Orders</dt>--}}
                        {{--@if (strpos($random_key,'5'))--}}
                        {{--<dd><a href="{{url('article/3')}}" title="Product Review">Product--}}
                                {{--Review</a></dd>--}}
                        {{--@endif--}}
                        {{--@if (strpos($random_key,'6'))--}}
                        {{--<dd><a href="{{url('article/4')}}"--}}
                               {{--title="Product Inquiry">Product Inquiry</a></dd>--}}
                        {{--@endif--}}
                        {{--@if (strpos($random_key,'7'))--}}
                        {{--<dd><a href="{{url('article/5')}}"--}}
                               {{--title="How to Place Order">How to Place Order</a></dd>--}}
                        {{--@endif--}}
                        {{--@if (strpos($random_key,'8'))--}}
                        {{--<dd><a href="{{url('article/6')}}"--}}
                               {{--title="24*7 Online Service">24*7 Online Service</a></dd>--}}
                        {{--@endif--}}
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/color-chart_a0016.html" title="Color Chart">Color--}}
                                {{--Chart</a></dd>--}}
                    {{--</dl>--}}
                    {{--@endif--}}
                    {{--<dl>--}}
                        {{--<dt>Payment</dt>--}}
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/returing-for-repleacemrnt-and-refund_a0029.html"--}}
                               {{--title="Returing for Repleacemrnt and Refund">Returing for Repleacemrnt and Refund</a>--}}
                        {{--</dd>--}}
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/return-policy_a0028.html" title="Return Policy">Return--}}
                                {{--Policy</a></dd>--}}
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/paymet-terms_a0008.html" title="Paymet Terms">Paymet--}}
                                {{--Terms</a></dd>--}}
                    {{--</dl>--}}

                    {{--<dl>--}}
                    {{--<dt>Help Center</dt>--}}
                    {{--<dd><a href="{{url('article/7')}}"--}}
                    {{--title="Conditions of Use">Conditions of Use</a>--}}
                    {{--</dd>--}}
                    {{--</dl>--}}
                </div>
                <div class="clear"></div>
            </div>
            <div class="bot">
                {{--<div class="copyright"> &nbsp;&nbsp;&nbsp;&nbsp; <a href="#" target="_blank">POWERED--}}
                        {{--BY WUXIAO</a></div>--}}
            </div>
        </div>
    </div>
</div>
{{--<div class="chattrans chatfloat4">--}}
    {{--<div id="service_3">--}}
        {{--<div class="sert"><img src="/Wedding Dress_files/bg4_0.png"></div>--}}
        {{--<div class="r r1 Color" style=" background-color:#FF7372;">--}}
            {{--<a href="skype:3214879?chat" target="_blank" title="Mickey">Mickey</a>--}}
            {{--<a href="#" target="_blank" title="Mickey">Mickey</a>--}}
        {{--</div>--}}
        {{--<div class="r r4 Color" style=" background-color:#FF7372;">--}}
            {{--<a href="javascript:void(0);" target="_blank" title="Lily">Lily</a>--}}
            {{--<span class="relimg"><img src="/"></span>--}}
        {{--</div>--}}
        {{--<div class="r r0 Color" style=" background-color:#FF7372;">--}}
            {{--<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=123456789&amp;site=qq&amp;menu=yes" target="_blank" title="Lee">Lee</a>--}}
            {{--<a href="#" target="_blank" title="Lee">Lee</a>--}}
        {{--</div>--}}
        {{--<div class="r top Color" style=" background-color:#FF7372;"><a--}}
                    {{--href="/">TOP</a></div>--}}
    {{--</div>--}}
{{--</div>--}}

<script type="text/javascript">
    $('.chattrans').removeClass('chathide');
    $('#chat_float_btn').remove();
</script>
{{--<div align="center">--}}
    {{--<script type="text/javascript" src="/Wedding Dress_files/analytics.js"></script>--}}
    {{--<script src="/Wedding Dress_files/fixed.toper.js"></script>--}}
{{--</div>--}}
{{--<iframe src="/Wedding Dress_files/source.html" style="display:none;"></iframe>--}}
</body>
</html>