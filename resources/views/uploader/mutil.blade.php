<div class="queueList">
    <div id="dndArea" class="placeholder">
        <div id="filePicker"></div>
        <p>或将照片拖到这里，单次最多可选300张</p>
    </div>
</div>
<div class="statusBar" style="display:none;">
    <div class="progress">
        <span class="text">0%</span>
        <span class="percentage"></span>
    </div><div class="info"></div>
    <div class="btns">
        <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
    </div>
</div>
<div id="uploaded" style="display:none;">
</div>

<link href="<?=$this->url?>/<?=$this->appName?>/webuploader/webuploader.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?=$this->url?>/<?=$this->appName?>/webuploader/upload.css" rel="stylesheet" type="text/css" media="screen" />
<script src="<?=$this->url?>/<?=$this->appName?>/webuploader/webuploader.min.js" charset="UTF-8" type="text/javascript"></script>
<script src="<?=$this->url?>/<?=$this->appName?>/webuploader/upload.js" charset="UTF-8" type="text/javascript"></script>
<script>
    var uploadeds = <?=json_encode($this->values)?>;
    $(function(){
        for (var i=0 ; i<uploadeds.length ; i++){
            ;
        }
    });
    /**
     * 图片上传成功后在form表单中的hidden元素记录图片
     * @param {type} file
     * @param {string} response ajax上传返回信息
     */
    function uploadSuccess(file , response){
        if (typeof response != 'object'){
                response = eval('('+response+')');
        }
        var images = response.data;
        $('#uploaded').append('<input type="hidden" name="<?=$this->name?>[]" value="'+images.uri+'" />');
    }
</script>