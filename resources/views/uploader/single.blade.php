{!! $pre !!}
<div id="uploader_{{$id}}">
    <!--用来存放item-->
    <div class="uploader-list"></div>
    <div id="filePicker_{{$id}}">选择图片</div>
</div>
<input id="uploaded_{{$id}}" type="hidden" name="{{$name}}" value="{{$value}}" />

<script>
    var disable = function(){
        return false;
    };
    var uploadConfig = {
        formData: {
            _token:'{{csrf_token()}}'
        },
        // 文件接收服务端。
        server: '{{url("ajax/upload/uploadimg")}}',

        wrap: '#uploader_{{$id}}',
        pick: '#filePicker_{{$id}}',
        /**
        * 图片上传成功后在form表单中的hidden元素记录图片
        * @param {type} file
        * @param {string} response ajax上传返回信息
        */
        uploadSuccess: function(file , response){
            if (typeof response != 'object'){
                    response = eval('('+response+')');
            }
            var images = response.data;
            $('#uploaded_{{$id}}').val(images.uri);
            $('#upload_button').unbind('click',disable);
        },
        /**
         * 上传过程中
         */
        uploadPending: function(file){
            $('#upload_button').bind('click',disable);
        },
        /**
         * 图片上传失败
         */
        uploadFail: function(file){
            $('#upload_button').unbind('click',disable);
        }
    };
</script>
<script src="{{asset('js/webuploader/uploadsingle.js')}}" charset="UTF-8" type="text/javascript"></script>