<div id="uploader">
    <!--用来存放item-->
    <div class="uploader-list"></div>
    <div id="filePicker">选择图片</div>
</div>
<input id="uploaded" type="hidden" name="{{$name}}" value="{{$value}}" />

<link href="{{asset('js/webuploader/webuploader.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{asset('js/webuploader/upload.css')}}" rel="stylesheet" type="text/css" media="screen" />
<script src="{{asset('js/webuploader/webuploader.min.js')}}" charset="UTF-8" type="text/javascript"></script>
<script src="{{asset('js/webuploader/uploadlogo.js')}}" charset="UTF-8" type="text/javascript"></script>
<script>
    uploadConfig = $.extend(uploadConfig,{
        formData: $.extend(uploadConfig.formData,{
            _token:'{{csrf_token()}}'}
        ),
        // 文件接收服务端。
        server: '{{url("ajax/upload/uploadimg")}}',
    });

    var disable = function(){
        return false;
    };
    /**
     * 上传过程中
     */
    function uploadPending(file){
        $('#upload_button').bind('click',disable);
    }
    /**
     * 图片上传成功后在form表单中的hidden元素记录图片
     * @param {type} file
     * @param {string} response ajax上传返回信息
     */
    function uploadSuccess(file , response){
        if (typeof response != 'object'){
                response = eval('('+response+')');
        }
        var images = response.data;
        $('#uploaded').val(images.uri);
        $('#upload_button').unbind('click',disable);
    }
    
    /**
     * 图片上传失败
     */
    function uploadFail(file){
        $('#upload_button').unbind('click',disable);
    }
</script>