@extends('admin.layouts.template')

@section('title', '管理后台')

@section('content')

    <script>
        $(function(){

            var form = new Vue({
                el:'#contentpanel',
                data:{
                },
                computed:{
                },
                mounted:function(){
                },
                watch: {
                },
                methods:{
                },
                components:{
                }
            });

        });

    </script>

    <div class="panel-body">
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="" method="GET" class="form-inline">
                    <div>
                        {!! $filter !!}
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="contentpanel" id="contentpanel">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table mb30">
                        <thead>
                        <tr style="white-space:nowrap">
                            <th>ASIN</th>
                            <th>图片</th>
                            <th>价格</th>
                            <th>商品链接</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row):?>
                        <tr data-id="{{$row->id}}">
                            <td>{{$row->asin}}</td>
                            <td><img src="{{$row->content['LargeImage']['URL']}}" width="100"/></td>
                            <td>{{@$row->content['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'] ? : @$row->content['OfferSummary']['LowestNewPrice']['FormattedPrice']}}</td>
                            <td><a href="{{$row->content['DetailPageURL']}}" target="_blank">点击打开</a></td>
                            <td>
                                {{--<button type="button" class="btn btn-warning">修改余额</button>--}}
                                {{--<a href="#" class="btn btn-white">查看</a>--}}
                                {{--<a onclick="return confirm('确定')" href="#" class="btn btn-danger">删除</a>--}}
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
                {{ @!$list->isEmpty() ? $list->appends(request()->all())->links() : '' }}
            </div>
            <!-- col-md-12 -->
        </div>
    </div><!-- contentpanel -->


@endsection

