@extends('admin.layouts.template')

@section('title', '管理后台')

@section('content')

    <script>
        $(function(){

            var form = new Vue({
                el:'#contentpanel',
                data:{
                },
                computed:{
                },
                mounted:function(){
                },
                watch: {
                },
                methods:{
                    add2activity:function(event){
                        var that = event.target;

                        var item_id = $(that).attr('data-id');
                        if (!item_id){
                            alert("缺少商品ID");
                            return false;
                        }

                        var activity_id = $('#activity_id').val();
                        if (!activity_id){
                            alert("请先选择一个集合以添加商品进来");
                            $('#activity_id').focus();
                            return false;
                        }

                        axios.post('/admin/product/add2activity', {activity_id:activity_id,item_id:item_id})
                            .then(function (response) {
                                if (response.data.code == 200) {
                                    alert('已添加成功');
                                } else {
                                    alert(response.data.message || '添加失败');
                                }
                            },function (response) {
                                alert(response.data.message || '添加失败');

                            }).catch(function (err) {
                            alert('添加失败');
                        });
                    }
                },
                components:{
                }
            });

        });

    </script>

    <div class="panel-body">
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="" method="GET" class="form-inline">
                    <div>
                        {!! $filter !!}
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="contentpanel" id="contentpanel">

        <div class="form-group">
            <label>
                <select id="activity_id" class="select-basic width300 select2-offscreen">
                    <option value="">请先选择一个集合以添加商品进来</option>
                    @foreach ($activity_list as $activity)
                        <option value="{{$activity->id}}">{{$activity->name}}</option>
                    @endforeach
                </select>
            </label>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table mb30">
                        <thead>
                        <tr style="white-space:nowrap">
                            <th>ID</th>
                            <th>ASIN</th>
                            <th>图片</th>
                            <th>价格</th>
                            <th>商品链接</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row):?>
                        <tr data-id="{{$row->id}}">
                            <td>{{$row->id}}</td>
                            <td>{{$row->asin}}</td>
                            <td><img src="{{$row->content['LargeImage']['URL']}}" width="100"/></td>
                            <td>{{@$row->content['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'] ? : @$row->content['OfferSummary']['LowestNewPrice']['FormattedPrice']}}</td>
                            <td><a href="{{$row->content['DetailPageURL']}}" target="_blank">点击打开</a></td>
                            <td>
                                <button type="button" class="btn btn-white" @click="add2activity" data-id="{{$row->id}}">添加到集合</button>
                                {{--<a href="#" class="btn btn-white">查看</a>--}}
                                {{--<a onclick="return confirm('确定')" href="#" class="btn btn-danger">删除</a>--}}
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
                {{ @!$list->isEmpty() ? $list->appends(request()->all())->links() : '' }}
            </div>
            <!-- col-md-12 -->
        </div>
    </div><!-- contentpanel -->


@endsection

