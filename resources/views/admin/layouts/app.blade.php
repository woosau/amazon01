<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Amazon管理后台</title>

    <link href="{{asset('dist/styles.css')}}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="signin">


<section>

    <div class="panel panel-signin">
        <div class="panel-body">
            <div class="logo text-center">
                <img src="images/logo-primary.png" alt="Chain Logo" >
            </div>
            <br />
            <h4 class="text-center mb5">登录您的后台账户</h4>
            @if (count($errors) > 0)
                <p class="text-center" style="color:red">{{ $errors->first() }}</p>
            @endif

            <div class="mb30"></div>

            <form action="{{ route('login') }}" method="post">
                {{ csrf_field() }}
                <div class="input-group mb15">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" class="form-control" placeholder="用户名" name="name" value="{{ old('name') }}" required autofocus>
                </div><!-- input-group -->
                <div class="input-group mb15">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" class="form-control" placeholder="密码" name="password" required>
                </div><!-- input-group -->

                <div class="clearfix">
                    <div class="pull-left">
                        <div class="ckbox ckbox-primary mt10">
                            <input type="checkbox" id="rememberMe" value="1" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="rememberMe">记住我</label>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">登录 <i class="fa fa-angle-right ml5"></i></button>
                    </div>
                </div>
            </form>

        </div><!-- panel-body -->
        {{--<div class="panel-footer">--}}
        {{--<a href="signup" class="btn btn-primary btn-block">Not yet a Member? Create Account Now</a>--}}
        {{--</div><!-- panel-footer -->--}}
    </div><!-- panel -->

</section>

</body>
</html>
