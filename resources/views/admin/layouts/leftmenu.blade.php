<div class="leftpanel">
    <div class="media profile-left">
        <a class="pull-left profile-thumb">
            <img class="img-circle" src="{{asset('images/photos/profile.png')}}" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">{{ Auth::user()->name }}</h4>
            <small class="text-muted">后台管理员</small>
        </div>
    </div><!-- media -->

    <h5 class="leftpanel-title">Navigation</h5>
    <ul class="nav nav-pills nav-stacked">
        @foreach (config('menu') as $title=>$main)
            @if (!is_array($main))
                <li><a href="{{url($main)}}" uri="{{$main}}"><i class="fa fa-home"></i> <span>{{$title}}</span></a></li>
            @else
                <li class="parent"><a href="javascript:void(0)"><i class="fa fa-bars"></i> <span>{{$title}}</span></a>
                    <ul class="children">
                        @foreach ($main as $title=>$sub)
                            <li><a href="{{url($sub)}}" uri="{{$sub}}">{{$title}}</a></li>
                        @endforeach
                    </ul>
                </li>
            @endif
        @endforeach

    </ul>

</div><!-- leftpanel -->
<script>
    $(function(){
        var uri = window.selectedMenu || '{{trim(parse_url(url()->current(),PHP_URL_PATH),'/') ? : '/'}}';
        var selectedMenu = $('a[uri="'+uri+'"]').parent('li');
        if (selectedMenu.length){
            var parentMenu = selectedMenu.parent('ul.children').parent('li.parent');
            if (parentMenu.length){
                selectedMenu.addClass('active');
                parentMenu.addClass('active');

                $('#crumb-main').text(parentMenu.find('span').text());
                $('#crumb-sub').text(selectedMenu.text());
            }else{
                selectedMenu.addClass('active');

                $('#crumb-sub').text(selectedMenu.text());
            }

        }
        if (window.selectedTitle){
            $('#crumb-sub').text(window.selectedTitle);
        }

    });
</script>