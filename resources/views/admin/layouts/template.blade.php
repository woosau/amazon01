<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{--{{$titlePrefix}} - --}}@yield('title')</title>

    {{--<link href="{{asset('css/style.default.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/morris.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/select2.css')}}" rel="stylesheet" />--}}
    <link href="{{asset('dist/styles.css')}}" rel="stylesheet" />
    <script type="text/javascript" src="{{asset('dist/common.js')}}"></script>
    <script type="text/javascript" src="{{asset('dist/global.js')}}"></script>
    <script type="text/javascript" src="{{asset('dist/theme.js')}}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body>

@include('admin.layouts.header')

<section>
    @yield('header')

    <div class="mainwrapper">
        @include('admin.layouts.leftmenu')

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="pageicon pull-left">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li id="crumb-main"></li>
                        </ul>
                        <h4 id="crumb-sub">Dashboard</h4>
                    </div>
                </div><!-- media -->
            </div><!-- pageheader -->

            @if (count($errors) > 0)
                <div class="panel-body">
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $error }}
                    </div>
                @endforeach
                </div>
            @endif

            @if ($success = session('success'))
                <div class="panel-body">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$success}}
                    </div>
                </div>
            @endif

            @if ($info = session('info'))
                <div class="panel-body">
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$info}}
                    </div>
                </div>
            @endif

            @if ($warning = session('warning'))
                <div class="panel-body">
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$warning}}
                    </div>
                </div>
            @endif

            @if ($alert = session('alert'))
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$alert}}
                    </div>
                </div>
            @endif

            @yield('content')<!-- contentpanel -->

        </div><!-- mainpanel -->
    </div><!-- mainwrapper -->

    @yield('footer')
</section>


{{--<script src="{{asset('js/jquery-migrate-1.2.1.min.js')}}"></script>--}}
{{--<script src="{{asset('js/jquery-ui-1.10.3.min.js')}}"></script>--}}
{{--<script src="{{asset('js/bootstrap.min.js')}}"></script>--}}
<script src="{{asset('js/modernizr.min.js')}}"></script>
{{--<script src="{{asset('js/pace.min.js')}}"></script>--}}
{{--<script src="{{asset('js/retina.min.js')}}"></script>--}}
{{--<script src="{{asset('js/jquery.cookies.js')}}"></script>--}}

{{--<script src="{{asset('js/jquery.autogrow-textarea.js')}}"></script>--}}
{{--<script src="{{asset('js/jquery.mousewheel.js')}}"></script>--}}
{{--<script src="{{asset('js/jquery.tagsinput.min.js')}}"></script>--}}
{{--<script src="{{asset('js/toggles.min.js')}}"></script>--}}
{{--<script src="{{asset('js/bootstrap-timepicker.min.js')}}"></script>--}}
{{--<script src="{{asset('js/jquery.maskedinput.min.js')}}"></script>--}}
{{--<script src="{{asset('js/colorpicker.js')}}"></script>--}}
{{--<script src="{{asset('js/dropzone.min.js')}}"></script>--}}

{{--<script src="{{asset('js/flot/jquery.flot.min.js')}}"></script>--}}
{{--<script src="{{asset('js/flot/jquery.flot.resize.min.js')}}"></script>--}}
{{--<script src="{{asset('js/flot/jquery.flot.spline.min.js')}}"></script>--}}
{{--<script src="{{asset('js/jquery.sparkline.min.js')}}"></script>--}}
{{--<script src="{{asset('js/morris.min.js')}}"></script>--}}
{{--<script src="{{asset('js/raphael-2.1.0.min.js')}}"></script>--}}
{{--<script src="{{asset('js/bootstrap-wizard.min.js')}}"></script>--}}
{{--<script src="{{asset('js/select2.min.js')}}"></script>--}}

{{--<script src="{{asset('js/custom.js')}}"></script>--}}
{{--<script src="{{asset('js/dashboard.js')}}"></script>--}}
<script>
    //表格全选反选
    function checkall(obj) {
        checkboxs = $(obj).parents('table').eq(0).find('input:checkbox');
        for (i in checkboxs) {
            checkboxs.get(i).checked = obj.checked;
        }
    }

    $(function(){
        jQuery(".select-basic, .select-multi").select2();
        jQuery('.select-search-hide').select2({
            minimumResultsForSearch: -1
        });
    });
</script>

</body>
</html>
