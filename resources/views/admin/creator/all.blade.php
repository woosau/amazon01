@extends('admin.layouts.template')

@section('title', '管理后台')

@section('content')

    <script>
        $(function(){

            var form = new Vue({
                el:'#contentpanel',
                data:{
                },
                computed:{
                },
                mounted:function(){
                },
                watch: {
                },
                methods:{
                },
                components:{
                }
            });

        });

    </script>

    <div class="panel-body">
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="" method="GET" class="form-inline">
                    <div>
                        {!! $filter !!}
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="contentpanel" id="contentpanel">

        <div class="form-group">
            <label>
                <a class="btn btn-primary" href="{{url('admin/creator/add')}}">+ 添加创作者</a>
            </label>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table mb30">
                        <thead>
                        <tr style="white-space:nowrap">
                            <th>名称</th>
                            <th>头像图</th>
                            <th>简介</th>
                            <th>创建时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row):?>
                        <tr data-id="{{$row->id}}">
                            <td>{{$row->name}}</td>
                            <td>
                                @if (!empty($row->image_url))
                                    <img width="100" src="{{$row->image_url}}">
                                @else
                                    /
                                @endif
                            </td>
                            <td>{{strip_tags($row->description)}}</td>
                            <td>{{$row->add_time}}</td>
                            <td>
                                {{--<button type="button" class="btn btn-warning">修改余额</button>--}}
                                <a href="{{url("admin/creator/edit/{$row->id}")}}" class="btn btn-white">编辑</a>
                                <a onclick="return confirm('确定')" href="{{url("admin/creator/del/{$row->id}")}}" class="btn btn-danger">删除</a>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
                {{ @!$list->isEmpty() ? $list->appends(request()->all())->links() : '' }}
            </div>
            <!-- col-md-12 -->
        </div>
    </div><!-- contentpanel -->


@endsection

