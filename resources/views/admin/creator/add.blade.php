@extends('admin.layouts.template')

@section('title', '管理后台')

@section('content')

    <script type="text/javascript" charset="utf-8" src="{{asset('js/ueditor/ueditor.config.js')}}"></script>
    <script type="text/javascript" charset="utf-8" src="{{asset('js/ueditor/ueditor.all.min.js')}}"> </script>
    <script>
        var selectedMenu = 'admin/creator/all';
        var selectedTitle = '添加创作者';

        $(function(){

            var form = new Vue({
                el:'#add',
                data:{
                },
                computed:{
                },
                mounted:function(){
                },
                watch: {
                },
                methods:{
                },
                components:{
                }
            });

            var ue = UE.getEditor('editor',{
                toolbars: [[
                    'fullscreen', 'source', '|', 'undo', 'redo', '|',
                    'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
                    'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                    'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                    'directionalityltr', 'directionalityrtl', 'indent', '|',
                    'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                    'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                    'simpleupload', 'emotion', 'scrawl', 'attachment', 'map', 'insertframe', 'pagebreak', 'template', 'background', '|',
                    'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
                    'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
                    'print', 'preview', 'searchreplace', 'drafts', 'help'
                ]],
                initialFrameWidth:600,
                initialFrameHeight:200
            });

            var validator = $("#form").validate({
                rules: {
                    name:{
                        required:true,
                        minlength:1,
                        maxlength:200,
                    },
                },
                messages: {
                    name:{
                        required:'请填写名称',
                        maxlength:'不要超过200个字',
                    },
                },
                submitHandler: function(form) {
                    form.submit();
                },
            });

        });

    </script>

    <div class="contentpanel" id="contentpanel">
        <div class="row">
            <div class="col-md-9">
                <form id="form" method="POST" action="" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <!-- 左侧表单 begin-->
                            <div class="col-md-6">
                                <div class="row">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">名称： <span class="asterisk">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="name" class="form-control" required="required" value="{{ old('name') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">封面图： </label>
                                        <div class="col-sm-9">
                                            <?=$uploader?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">描述： </label>
                                        <div class="col-sm-9">
                                            {{--<textarea rows="5" name="content" class="form-control"></textarea>--}}
                                            <script id="editor" type="text/plain"  name="description">{!! old('description') !!}</script>
                                        </div>
                                    </div>

                                </div><!-- row -->
                            </div>
                            <!-- 左侧表单 end-->
                        </div><!-- panel-body -->
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button class="btn btn-primary mr5" type="submit" id="upload_button">保存</button>
                                    <a href="{{$_refer}}" class="btn btn-dark">取消</a>
                                </div>
                            </div>
                        </div><!-- panel-footer -->
                    </div><!-- panel -->
                </form>
            </div>
        </div>
    </div>


@endsection

