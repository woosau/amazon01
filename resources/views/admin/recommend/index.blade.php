@extends('admin.layouts.template')

@section('title', '管理后台')

@section('content')

    <script>
//        var selectedMenu = 'admin/activity/all';
        var selectedTitle = '编辑首页推荐';

        $(function(){

            var form = new Vue({
                el:'#contentpanel',
                data:{
                },
                computed:{
                },
                mounted:function(){
                },
                watch: {
                },
                methods:{
                    add_item:function(event){
                        var item_id = $('#item_id').val();
                        if (!item_id){
                            return;
                        }
                        var item = $('#item_id option:selected');
                        $('#products').append('<div class="form-group" id="item_'+item_id+'">' +
                            '<input type="hidden" name="item_id[]" value="'+item_id+'"/>' +
                            '<label class="col-sm-3 control-label"><button class="btn btn-danger mr5" type="button" onclick="if(confirm(\'确定删除\'))$(\'#item_'+item_id+'\').remove();">删除商品</button></label>' +
                            '<div class="col-sm-9">' +
                            item.attr('data-asin')+'('+item.attr('data-price')+')' +
                            '<img src="'+item.attr('data-image')+'" width="100px"/>' +
                            '</div>' +
                            '</div>');
                    }
                },
                components:{
                }
            });

            var validator = $("#form").validate({
                rules: {
                },
                messages: {
                },
                submitHandler: function(form) {
                    form.submit();
                },
            });

        });

    </script>

    <div class="contentpanel" id="contentpanel">
        <div class="row">
            <div class="col-md-9">
                <form id="form" method="POST" action="" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <!-- 左侧表单 begin-->
                            <div class="col-md-6">
                                <div class="row">

                                    <div id="products">
                                        @foreach ($items as $row)
                                            <div class="form-group" id="item_{{$row->id}}">
                                                <input type="hidden" name="item_id[]" value="{{$row->id}}"/>
                                                <label class="col-sm-3 control-label"><button class="btn btn-danger mr5" type="button" onclick="if(confirm('确定删除'))$('#item_{{$row->id}}').remove();">删除商品</button></label>
                                                <div class="col-sm-9">
                                                    {{$row->asin}}({{$row->price_text}})
                                                    <img src="{{$row->image_url}}" width="100px"/>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><button class="btn btn-primary mr5" type="button" @click="add_item">添加商品</button> </label>
                                        <div class="col-sm-9">
                                            <select id="item_id" class="select-basic width300 select2-offscreen">
                                                <option value="">请选择</option>
                                                @foreach ($products as $row)
                                                <option value="{{$row->id}}" data-asin="{{$row->asin}}" data-price="{{$row->price_text}}" data-image="{{$row->image_url}}">{{$row->asin}}({{$row->price_text}})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div><!-- row -->
                            </div>
                            <!-- 左侧表单 end-->
                        </div><!-- panel-body -->
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button class="btn btn-primary mr5" type="submit" id="upload_button">保存</button>
                                    {{--<a href="{{$_refer}}" class="btn btn-dark">取消</a>--}}
                                </div>
                            </div>
                        </div><!-- panel-footer -->
                    </div><!-- panel -->
                </form>
            </div>
        </div>
    </div>


@endsection

