@extends('admin.layouts.template')

@section('title', '管理后台')

@section('content')

    <script>
        $(function(){

            var form = new Vue({
                el:'#contentpanel',
                data:{
                },
                computed:{
                },
                mounted:function(){
                },
                watch: {
                },
                methods:{
                },
                components:{
                }
            });

        });

    </script>

    <div class="panel-body">
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="" method="GET" class="form-inline">
                    <div>
                        {!! $filter !!}
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="contentpanel" id="contentpanel">

        <div class="form-group">
            <label>
                <a class="btn btn-primary" href="{{url('admin/activity/add')}}">+ 添加集合</a>
            </label>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table mb30">
                        <thead>
                        <tr style="white-space:nowrap">
                            <th>名称</th>
                            <th>位置</th>
                            <th>封面图</th>
                            <th>商品数量</th>
                            <th>创建时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row):?>
                        <tr data-id="{{$row->id}}">
                            <td>{{$row->name}}</td>
                            <td>{{$row->position}}</td>
                            <td>
                                @if (!empty($row->image_url))
                                    <img width="100" src="{{$row->image_url}}">
                                @else
                                    /
                                @endif
                            </td>
                            <td>{{$row->item_count}}</td>
                            <td>{{$row->add_time}}</td>
                            <td>
                                {{--<button type="button" class="btn btn-warning">修改余额</button>--}}
                                <a href="{{url("admin/activity/edit/{$row->id}")}}" class="btn btn-white">编辑</a>
                                <a onclick="return confirm('确定')" href="{{url("admin/activity/del/{$row->id}")}}" class="btn btn-danger">删除</a>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
                {{ @!$list->isEmpty() ? $list->appends(request()->all())->links() : '' }}
            </div>
            <!-- col-md-12 -->
        </div>
    </div><!-- contentpanel -->


@endsection

