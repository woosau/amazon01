<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="shortcut icon" href="/Wedding Dress_files/1f6a23397e.png">
    <meta name="keywords" content="Wedding Dress">
    <meta name="description" content="Wedding Dress">
    <title>Wedding Dress</title>
    <link href="/Wedding Dress_files/global.css" rel="stylesheet" type="text/css">
    <link href="/Wedding Dress_files/global(1).css" rel="stylesheet" type="text/css">
    <link href="/Wedding Dress_files/user.css" rel="stylesheet" type="text/css">
    <link href="/Wedding Dress_files/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/Wedding Dress_files/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/Wedding Dress_files/en.js"></script>
    <script type="text/javascript" src="/Wedding Dress_files/global.js"></script>
    <script type="text/javascript" src="/Wedding Dress_files/global.js(1)"></script>
    <script type="text/javascript" src="/Wedding Dress_files/user.js"></script>
    <script type="text/javascript" src="/Wedding Dress_files/main.js"></script>
    <link href="/Wedding Dress_files/font.css" rel="stylesheet" type="text/css">    <!-- Facebook Pixel Code -->
    <!-- End Facebook Pixel Code -->
    <link href="/Wedding Dress_files/index.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="paypal" data-requiremodule="login"
            src="/Wedding Dress_files/login.js"></script>
    <style type="text/css">/*!reset via github.com/premasagar/cleanslate*/
        .LIwPP,
        .LIwPP b,
        .LIwPP img,
        .LIwPP svg {
            -webkit-box-sizing: content-box !important;
            -moz-box-sizing: content-box !important;
            box-sizing: content-box !important;
            background-attachment: scroll !important;
            background-color: transparent !important;
            background-image: none !important;
            background-position: 0 0 !important;
            background-repeat: repeat !important;
            border-color: black !important;
            border-color: currentColor !important;
            border-radius: 0 !important;
            border-style: none !important;
            border-width: medium !important;
            direction: inherit !important;
            display: inline !important;
            float: none !important;
            font-size: inherit !important;
            height: auto !important;
            letter-spacing: normal !important;
            line-height: inherit !important;
            margin: 0 !important;
            max-height: none !important;
            max-width: none !important;
            min-height: 0 !important;
            min-width: 0 !important;
            opacity: 1 !important;
            outline: invert none medium !important;
            overflow: visible !important;
            padding: 0 !important;
            position: static !important;
            text-align: inherit !important;
            text-decoration: inherit !important;
            text-indent: 0 !important;
            text-shadow: none !important;
            text-transform: none !important;
            unicode-bidi: normal !important;
            vertical-align: baseline !important;
            visibility: inherit !important;
            white-space: normal !important;
            width: auto !important;
            word-spacing: normal !important;
        }

        .LIwPP *[dir=rtl] {
            direction: rtl !important;
        }

        .LIwPP {
            direction: ltr !important;
            font-style: normal !important;
            text-align: left !important;
            text-decoration: none !important;
        }

        .LIwPP {
            background-color: #bfbfbf !important;
            background-image: -webkit-linear-gradient(#e0e0e0 20%, #bfbfbf) !important;
            background-image: linear-gradient(#e0e0e0 20%, #bfbfbf) !important;
            background-repeat: no-repeat !important;
            border: 1px solid #bbb !important;
            border-color: #cbcbcb #b2b2b2 #8b8b8b !important;
            -webkit-box-shadow: inset 0 1px #ececec !important;
            box-shadow: inset 0 1px #ececec !important;
            -webkit-box-sizing: border-box !important;
            -moz-box-sizing: border-box !important;
            box-sizing: border-box !important;
            border-radius: 5px !important;
            font-size: 16px !important;
            height: 2em !important;
            margin: 0 !important;
            overflow: hidden !important;
            padding: 0 !important;
            position: relative !important;
            text-align: center !important;
            width: auto !important;
            white-space: nowrap !important;
        }

        .LIwPP,
        .LIwPP b,
        .LIwPP i {
            cursor: pointer !important;
            display: inline-block !important;
            -webkit-user-select: none !important;
            -moz-user-select: none !important;
            -ms-user-select: none !important;
            user-select: none !important;
        }

        .LIwPP b {
            border-left: 1px solid #b2b2b2 !important;
            -webkit-box-shadow: inset 1px 0 rgba(255, 255, 255, .5) !important;
            box-shadow: inset 1px 0 rgba(255, 255, 255, .5) !important;
            color: #333333 !important;
            font: normal 700 0.6875em/1.5 "Helvetica Neue", Arial, sans-serif !important;
            height: 2.25em !important;
            padding: .625em .66667em 0 !important;
            text-shadow: 0 1px 0 #eee !important;
            vertical-align: baseline !important;
        }

        .LIwPP .PPTM,
        .LIwPP i {
            height: 100% !important;
            margin: 0 .4em 0 .5em !important;
            vertical-align: middle !important;
            width: 1em !important;
        }

        .LIwPP i {
            background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAhCAMAAADnC9tbAAAA81BMVEX///////+ozeUAecEARXyozeUARXwAecH///8AecGozeX///8ARXz///+ozeUAecEARXzg7fYAbrPx9/v///8AecFGntKozeUAZqgARXwARXy11OmFv+EAecEAaav///8AecGozeUARXwAW5rZ6fTg7fb5+/3V5/LA2+z///8ARXyozeW92euy0+j5+/0AecEAc7kAbLAASIDR5fH///+ozeUAY6MAT4oARXwAecEARXz///+ozeX///8ARXwAecGozeUARXyozeX///8AecH///8AecEARXyozeWozeX///8ARXwAecGozeUAecH///8ARXwU3q5BAAAATXRSTlMAERERESIiIiIzMzMzRERERERERFVVVVVVVWZmZmZmZnd3d3d3d3d3d4iIiIiIiIiIiIiImZmZmZmqqqqqu7u7u8zMzMzd3d3d7u7u7nVO37gAAAEZSURBVHheTY5rX4IwGEf/aFhWoJV2sRugXcDu2QUMDA0QbdPv/2naw9B13jzb2X5nA1CbLyVxBwWd5RoPhLdU1EjEQjiAQ+K9DWysTmh2f4CmmBnKG89cJrLmySftI1+ISCXnx9wH1D7Y/+UN7JLIoih4uRhxfi5bsTUapZxzvw4goA/y1LKsRhVERq/zNkpk84lXlXC8j26aQoG6qD3iP5uHZwaxg5KtRcnM1UBcLtYMQQzoMJwUZo9EIkQLMEi82oBGC62cvSnQEjMB4JK4Y3KRuG7RGHznQKgemZyyN2ChvnHPcl3Gw9B9uOpPWb4tE8MvRkztCoChENdsfGSaOgpmQtwwE2uo1mcVJYyD3m0+hgJ6zpitxB/jvlwEGmtLjgAAAABJRU5ErkJggg==') no-repeat !important;
            height: 1em !important;
        }

        .LIwPP img.PPTM {
            height: auto !important;
        }

        .LIwPP:hover,
        .LIwPP:active {
            background-color: #a5a5a5 !important;
            background-image: -webkit-linear-gradient(#e0e0e0 20%, #a5a5a5) !important;
            background-image: linear-gradient(#e0e0e0 20%, #a5a5a5) !important;
            background-repeat: no-repeat !important;
        }

        .LIwPP:active {
            border-color: #8c8c8c #878787 #808080 !important;
            -webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.2) !important;
            box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.2) !important;
            outline: 0 !important;
        }

        .LIwPP:hover b {
            -webkit-box-shadow: inset 1px 0 rgba(255, 255, 255, .3) !important;
            box-shadow: inset 1px 0 rgba(255, 255, 255, .3) !important;
        }

        .PPBlue {
            background-color: #0079c1 !important;
            background-image: -webkit-linear-gradient(#00a1ff 20%, #0079c1) !important;
            background-image: linear-gradient(#00a1ff 20%, #0079c1) !important;
            background-repeat: no-repeat !important;
            border-color: #0079c1 #00588b #004b77 !important;
            -webkit-box-shadow: inset 0 1px #4dbeff !important;
            box-shadow: inset 0 1px #4dbeff !important;
        }

        .PPBlue b {
            -webkit-box-shadow: inset 1px 0 rgba(77, 190, 255, .5) !important;
            box-shadow: inset 1px 0 rgba(77, 190, 255, .5) !important;
            border-left-color: #00588b !important;
            color: #f9fcff !important;
            -webkit-font-smoothing: antialiased !important;
            font-smoothing: antialiased !important;
            text-shadow: 0 -1px 0 #00629c !important;
        }

        .PPBlue i {
            background-position: 0 100% !important;
        }

        .PPBlue .PPTM-btm {
            fill: #a8cde5 !important;
        }

        .PPBlue .PPTM-top {
            fill: #fff !important;
        }

        .PPBlue:hover,
        .PPBlue:active {
            background-color: #005282 !important;
            background-image: -webkit-linear-gradient(#0083cf 20%, #005282) !important;
            background-image: linear-gradient(#0083cf 20%, #005282) !important;
            background-repeat: no-repeat !important;
            border-color: #006699 #004466 #003355 !important;
        }

        .PPBlue:hover b,
        .PPBlue:active b {
            text-shadow: 0 -1px 0 #004466 !important;
        }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-35990925-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-35990925-2');
    </script>
</head>

<body class="lang_en w_1200" style="">
<link type="text/css" rel="stylesheet" href="/Wedding Dress_files/open.css">
<style type="text/css">
    .FontColor, a.FontColor, a.FontColor:hover, a:hover {
        color: #EA5765;
    }

    .FontBgColor {
        background-color: #EA5765;
    }

    .FontBorderColor {
        border-color: #EA5765;
    }

    .FontBorderHoverColor:hover, a.FontBorderHoverColor:hover {
        border-color: #EA5765;
    }

    .FontBgHoverColor:hover {
        background-color: #EA5765 !important;
    }

    .FontHoverColor:hover {
        color: #EA5765 !important;
    }

    .NavBgColor {
        background-color: #FFFFFF;
    }

    .NavHoverBgColor:hover {
        background-color: #000000;
    }

    .NavBorderColor1 {
        border-color: #030303;
    }

    .NavBorderColor2 {
        border-color: #0F0F0F;
    }

    .PriceColor {
        color: #1C1C1C;
    }

    .AddtoCartBgColor {
        background-color: #D15666;
    }

    .AddtoCartBorderColor {
        border-color: #D15666;
    }

    .BuyNowBgColor {
        background-color: #EA215C;
    }

    .ReviewBgColor {
        background-color: #EA215C;
    }

    .DiscountBgColor {
        background-color: #EA215C;
    }

    .DiscountBorderColor {
        border-color: #EA215C;
    }

    .ProListBgColor {
        background-color: #B05F72;
    }

    .ProListHoverBgColor:hover {
        background-color: #BE7886;
    }

    .GoodBorderColor {
        border-color: #B01313;
    }

    .GoodBorderHoverColor:hover {
        border-color: #BE2E37;
    }

    .GoodBorderColor.selected {
        border-color: #BE2E37;
    }

    .GoodBorderBottomHoverColor {
        border-bottom-color: #BE2E37;
    }
</style>
<script type="text/javascript">
//    $(window).resize(function () {
//        $(window).webDisplay(0);
//    });
//    $(window).webDisplay(0);
//    var ueeshop_config = {
//        "domain": "http://t069.shop.ueeshop.com",
//        "date": "2017/11/28 13:55:41",
//        "lang": "en",
//        "currency": "USD",
//        "currency_symbols": "$",
//        "currency_rate": "1.0000",
//        "FbAppId": "1594883030731762",
//        "FbPixelOpen": "1",
//        "UserId": "0",
//        "TouristsShopping": "1",
//        "PaypalExcheckout": ""
//    }
</script>

<div id="header" class="wide">
    <div class="container">
        <div class="left_bar fl ">
            <ul class="crossn">
                <li class="block fl">
                    <div class="fl"><strong>Currency:</strong></div>
                    <dl class="fl ">
                        <dt><strong id="currency" class="FontColor">USD</strong></dt>
                        <dd class="currency">
                            <a rel="nofollow" href="javascript:;" data="USD"><img
                                        src="/Wedding Dress_files/7eed9f2e47.jpg" alt="USD">USD</a>
                            <a rel="nofollow" href="javascript:;" data="EUR"><img
                                        src="/Wedding Dress_files/a87887be78.jpg" alt="EUR">EUR</a>
                            <a rel="nofollow" href="javascript:;" data="GBP"><img
                                        src="/Wedding Dress_files/0d196ae885.jpg" alt="GBP">GBP</a>
                            <a rel="nofollow" href="javascript:;" data="CAD"><img
                                        src="/Wedding Dress_files/fc09866559.jpg" alt="CAD">CAD</a>
                            <a rel="nofollow" href="javascript:;" data="AUD"><img
                                        src="/Wedding Dress_files/21d159fb43.jpg" alt="AUD">AUD</a>
                            <a rel="nofollow" href="javascript:;" data="CHF"><img
                                        src="/Wedding Dress_files/ca080529a0.jpg" alt="CHF">CHF</a>
                            <a rel="nofollow" href="javascript:;" data="HKD"><img
                                        src="/Wedding Dress_files/986ff3624d.jpg" alt="HKD">HKD</a>
                            <a rel="nofollow" href="javascript:;" data="JPY"><img
                                        src="/Wedding Dress_files/802c1f1cd6.jpg" alt="JPY">JPY</a>
                            <a rel="nofollow" href="javascript:;" data="RUB"><img
                                        src="/Wedding Dress_files/e374f7aef1.jpg" alt="RUB">RUB</a>
                            <a rel="nofollow" href="javascript:;" data="CNY">CNY</a>
                            <a rel="nofollow" href="javascript:;" data="SAR">SAR</a>
                            <a rel="nofollow" href="javascript:;" data="SGD">SGD</a>
                            <a rel="nofollow" href="javascript:;" data="NZD">NZD</a>
                            <a rel="nofollow" href="javascript:;" data="ARS">ARS</a>
                            <a rel="nofollow" href="javascript:;" data="INR">INR</a>
                            <a rel="nofollow" href="javascript:;" data="COP">COP</a>
                            <a rel="nofollow" href="javascript:;" data="AED">AED</a>
                        </dd>
                    </dl>
                </li>
            </ul>
        </div>
        <div class="right_bar fr">
            <ul>
                <li class="icon">
                    {{--<div class="global_login_sec" style="display: block;">--}}
                        {{--<div class="SignInButton_sec"></div>--}}
                        {{--<div class="signin_box_sec global_signin_module" style="display: none;">--}}
                            {{--<div class="signin_container">--}}
                                {{--<form class="signin_form" name="signin_form" method="post">--}}
                                    {{--<input name="Email" class="g_s_txt" type="text" maxlength="100"--}}
                                           {{--placeholder="Address" format="Email" notnull="">--}}
                                    {{--<div class="blank20"></div>--}}
                                    {{--<input name="Password" class="g_s_txt" type="password" placeholder="Password"--}}
                                           {{--notnull="">--}}
                                    {{--<button class="signin FontBgColor" type="submit">SIGN IN</button>--}}
                                    {{--<input type="hidden" name="do_action" value="user.login">--}}
                                {{--</form>--}}
                                {{--<div id="error_login_box" class="error_login_box"></div>--}}
                                {{--<h4>Sign in with:</h4>--}}
                                {{--<ul>--}}
                                    {{--<script type="text/javascript" src="/Wedding Dress_files/facebook.js"></script>--}}
                                    {{--<li id="fb_button" scope="public_profile, email" onclick="checkLoginState();"--}}
                                        {{--appid="1594883030731762"></li>--}}
                                    {{--<script type="text/javascript" src="/Wedding Dress_files/google.js"></script>--}}
                                    {{--<li id="google_login">--}}
                                        {{--<div id="google_btn"--}}
                                             {{--clientid="640892902807-64ev2hjtu0141m7lsn9k15mfm9b5nak5.apps.googleusercontent.com">--}}
                                            {{--&nbsp;--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--<script type="text/javascript" src="/Wedding Dress_files/api.js"></script>--}}
                                    {{--<li id="paypalLogin"--}}
                                        {{--appid="AXaj3xAfsUrUujWbtErMA7ElMc6gEeou8TMJMibnisJR21oFmbZ-4eCrvx5d"--}}
                                        {{--u="http://t011.uee.ly200.net/?do_action=user.user_oauth&amp;Type=Paypal">--}}
                                        {{--<button id="LIwPP2829892" class="LIwPP PPBlue">--}}
                                            {{--<svg class="PPTM" xmlns="http://www.w3.org/2000/svg" version="1.1"--}}
                                                 {{--width="16px" height="17px" viewBox="0 0 16 17">--}}
                                                {{--<path class="PPTM-btm" fill="#0079c1"--}}
                                                      {{--d="m15.603 3.917c-0.264-0.505-0.651-0.917-1.155-1.231-0.025-0.016-0.055-0.029-0.081-0.044 0.004 0.007 0.009 0.014 0.013 0.021 0.265 0.506 0.396 1.135 0.396 1.891 0 1.715-0.712 3.097-2.138 4.148-1.425 1.052-3.418 1.574-5.979 1.574h-0.597c-0.45 0-0.9 0.359-1.001 0.798l-0.719 3.106c-0.101 0.438-0.552 0.797-1.002 0.797h-1.404l-0.105 0.457c-0.101 0.438 0.184 0.798 0.633 0.798h2.1c0.45 0 0.9-0.359 1.001-0.798l0.718-3.106c0.101-0.438 0.551-0.797 1.002-0.797h0.597c2.562 0 4.554-0.522 5.979-1.574 1.426-1.052 2.139-2.434 2.139-4.149 0-0.755-0.132-1.385-0.397-1.891z"></path>--}}
                                                {{--<path class="PPTM-top" fill="#00457c"--}}
                                                      {{--d="m9.27 6.283c-0.63 0.46-1.511 0.691-2.641 0.691h-0.521c-0.45 0-0.736-0.359-0.635-0.797l0.628-2.72c0.101-0.438 0.552-0.797 1.002-0.797h0.686c0.802 0 1.408 0.136 1.814 0.409 0.409 0.268 0.611 0.683 0.611 1.244 0 0.852-0.315 1.507-0.944 1.97zm3.369-5.42c-0.913-0.566-2.16-0.863-4.288-0.863h-4.372c-0.449 0-0.9 0.359-1.001 0.797l-2.957 12.813c-0.101 0.439 0.185 0.798 0.634 0.798h2.099c0.45 0 0.901-0.358 1.003-0.797l0.717-3.105c0.101-0.438 0.552-0.797 1.001-0.797h0.598c2.562 0 4.554-0.524 5.979-1.575 1.427-1.051 2.139-2.433 2.139-4.148-0.001-1.365-0.439-2.425-1.552-3.123z"></path>--}}
                                            {{--</svg>--}}
                                            {{--<b>Log In with PayPal</b></button>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                                {{--<div class="blank20"></div>--}}
                                {{--<a href="http://t069.shop.ueeshop.com/account/sign-up.html"--}}
                                   {{--class="signup FontBorderColor FontColor">JOIN FREE</a>--}}
                                {{--<a href="http://t069.shop.ueeshop.com/account/forgot.html" class="forgot">Forgot your--}}
                                    {{--password?</a>--}}
                                {{--<div class="clear"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<script type="text/javascript">--}}
                        {{--$(document).ready(function () {--}}
                            {{--user_obj.sign_in_init();--}}
                            {{--var timer;--}}
                            {{--$('.global_login_sec').hover(--}}
                                {{--function () {--}}
                                    {{--clearTimeout(timer);--}}
                                    {{--$(this).find('.SignInButton_sec').addClass('cur');--}}
                                    {{--$(this).find('.signin_box_sec').fadeIn();--}}
                                {{--},--}}
                                {{--function () {--}}
                                    {{--var _this = $(this);--}}
                                    {{--timer = setTimeout(function () {--}}
                                        {{--_this.find('.SignInButton_sec').removeClass('cur');--}}
                                        {{--_this.find('.signin_box_sec').fadeOut();--}}
                                    {{--}, 500);--}}
                                {{--}--}}
                            {{--);--}}
                        {{--});--}}
                    {{--</script>--}}
                </li>
                {{--<li class="icon">--}}
                    {{--<div class="header_cart" lang="_en"><a href="http://t069.shop.ueeshop.com/cart/" class="cart_inner"><span--}}
                                    {{--class="cart_count FontBgColor">0</span></a></div>--}}
                {{--</li>--}}
                <li class="icon icon_re">
                    <div class="search"><a href="javascript:;"></a></div>
                    <div class="search_form">
                        <form action="/search/" method="get" class="form">
                            <input type="text" class="text fl" placeholder="" name="Keyword" notnull="" value="">
                            <input type="submit" class="button fr" value="">
                            <div class="clear"></div>
                            <i></i>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        <div class="logo"><h1><a href="/"><img src="/Wedding Dress_files/3284a8ba19.png"
                                                                           alt="Trendy Furniture Store"></a></h1></div>
        <div class="clear"></div>
    </div>
</div>
<div id="nav_outer">
    <div class="wide">
        <div id="nav" class="container" style="overflow: visible;">
            <ul class="nav_item">
                <li>
                    <a href="/">Home</a>
                </li>
                {{--<li>--}}
                    {{--<a class="FontColor" href="http://t069.shop.ueeshop.com/products/">Products</a>--}}
                    {{--<div class="nav_sec">--}}
                        {{--<div class="nav_sec_box">--}}
                            {{--<div class="wide">--}}
                                {{--<div class="container">--}}
                                    {{--<div class="nav_sec_list">--}}
                                        {{--<a class="nav_sec_a" href="http://t069.shop.ueeshop.com/c/wedding-dresses_0419"--}}
                                           {{--title="Wedding Dresses">Wedding Dresses</a>--}}
                                        {{--<div class="nav_thd">--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/luxury-wedding-dresses_0423"--}}
                                                                         {{--title="Luxury Wedding Dresses">Luxury Wedding--}}
                                                    {{--Dresses</a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="nav_sec_list">--}}
                                        {{--<a class="nav_sec_a"--}}
                                           {{--href="http://t069.shop.ueeshop.com/c/special-occasion-dresses_0429"--}}
                                           {{--title="Special  Occasion  Dresses">Special Occasion Dresses</a>--}}
                                        {{--<div class="nav_thd">--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/prom-dresses_0430"--}}
                                                                         {{--title="Prom Dresses">Prom Dresses</a></div>--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/evening-dresses_0431"--}}
                                                                         {{--title="Evening Dresses">Evening Dresses</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="nav_sec_list">--}}
                                        {{--<a class="nav_sec_a" href="http://t069.shop.ueeshop.com/c/wedding-apparel_0434"--}}
                                           {{--title="Wedding Apparel">Wedding Apparel</a>--}}
                                        {{--<div class="nav_thd">--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/wedding-party-dresses_0435"--}}
                                                                         {{--title="Wedding Party Dresses">Wedding Party--}}
                                                    {{--Dresses</a></div>--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/junior-bridesmaid-dresses_0436"--}}
                                                                         {{--title="Junior Bridesmaid Dresses">Junior--}}
                                                    {{--Bridesmaid Dresses</a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="nav_sec_list">--}}
                                        {{--<a class="nav_sec_a" href="http://t069.shop.ueeshop.com/c/new-wedding_0444"--}}
                                           {{--title="New Wedding">New Wedding</a>--}}
                                        {{--<div class="nav_thd">--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/ball-gown-dresses_0445"--}}
                                                                         {{--title="Ball Gown Dresses">Ball Gown Dresses</a>--}}
                                            {{--</div>--}}
                                            {{--<div class="nav_thd_list"><a class="FontHoverColor"--}}
                                                                         {{--href="http://t069.shop.ueeshop.com/c/popular-prom-dresses_0446"--}}
                                                                         {{--title="Popular Prom Dresses">Popular Prom--}}
                                                    {{--Dresses</a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="clear"></div>--}}
                                    {{--<div class="clear"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/GroupBuying.html">Group Purchase</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/holiday.html">Holiday</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/FlashSale.html">Flash Sale</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/blog/">Blog</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="http://t069.shop.ueeshop.com/c/special-occasion-dresses_0429">Special Occasion Dresses</a>--}}
                {{--</li>--}}
                {{--<li style="display: none;">--}}
                    {{--<a href="http://t069.shop.ueeshop.com/c/wedding-dresses_0419">Wedding Dresses</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</div>

<div id="main" class="wide">
    <div class="blank20"></div>
    <div class="side_left fl">
        <div class="help_menu">
            <div class="help_title">Overview</div>
            <ul class="help_list">
                <li><a hidefocus="true" href="{{url('article/1')}}">Location &amp; Working Hours</a></li>
                {{--<li><a hidefocus="true" href="/art/about-rockliffeocom_a0019.html">About Rockliffeo.com</a></li>--}}
                <li><a hidefocus="true" href="{{url('article/2')}}">Our Guarantee</a></li>
                {{--<li><a hidefocus="true" href="/art/location-amp-working-hours_a0007.html">Location &amp; Working Hours</a></li>--}}
            </ul>
            <div class="help_title">Products &amp; Orders</div>
            <ul class="help_list">
                <li><a hidefocus="true" href="{{url('article/3')}}">Product Review</a></li>
                <li><a hidefocus="true" href="{{url('article/4')}}">Product Inquiry</a></li>
                <li><a hidefocus="true" href="{{url('article/5')}}">How to Place Order</a></li>
                <li><a hidefocus="true" href="{{url('article/6')}}">24*7 Online Service</a></li>
                {{--<li><a hidefocus="true" href="/art/color-chart_a0016.html">Color Chart</a></li>--}}
                {{--<li><a hidefocus="true" href="/art/measurement_a0015.html">Measurement</a></li>--}}
                {{--<li><a hidefocus="true" href="/art/size-chart_a0014.html">Size Chart</a></li>--}}
            </ul>
            {{--<div class="help_title">Payment</div>--}}
            {{--<ul class="help_list">--}}
                {{--<li><a hidefocus="true" href="/art/returing-for-repleacemrnt-and-refund_a0029.html">Returing for Repleacemrnt and Refund</a></li>--}}
                {{--<li><a hidefocus="true" href="/art/return-policy_a0028.html">Return Policy</a></li>--}}
                {{--<li><a hidefocus="true" href="/art/paymet-terms_a0008.html">Paymet Terms</a></li>--}}
            {{--</ul>--}}
            {{--<div class="help_title">Shipping &amp; Costs</div>--}}
            {{--<ul class="help_list">--}}
                {{--<li><a hidefocus="true" href="/art/shipping-cost-and-tax_a0030.html">Shipping Cost and Tax</a></li>--}}
                {{--<li><a hidefocus="true" href="/art/shipping-methods_a0004.html">Shipping Methods</a></li>--}}
                {{--<li><a hidefocus="true" href="/art/shipping-policy_a0003.html">Shipping Policy</a></li>--}}
            {{--</ul>--}}
            <div class="help_title">Help Center</div>
            <ul class="help_list">
                {{--<li><a hidefocus="true" href="/art/how-to-measure_a0034.html">How to Measure</a></li>--}}
                {{--<li><a hidefocus="true" href="/art/faqs_a0033.html">FAQS</a></li>--}}
                <li><a hidefocus="true" href="{{url('article/7')}}">Conditions of Use</a></li>
            </ul>
        </div>
    </div>
    <div class="side_right fr right_main">
        <div class="main_title">{{$title}}</div>
        <div class="main_content editor_txt">{!! $content !!}</div>
    </div>
    <div class="blank25"></div>
</div>

<div id="footer" class="min">
    <div class="wide">
        <div class="container">
            <div class="top">
                <div class="con fl">
                    <div class="logo">
                        <div><img src="/Wedding Dress_files/1901574b0e.png" alt=""></div>
                    </div>
                    {{--<div class="share">--}}
                        {{--<div class="follow_toolbox clearfix">--}}
                            {{--<ul>--}}
                                {{--<li><a rel="nofollow" class="follow_facebook" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Facebook">Facebook</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_twitter" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Twitter">Twitter</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_pinterest" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Pinterest">Pinterest</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_youtube" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="YouTube">YouTube</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_google" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Google">Google</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_vk" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="VK">VK</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_linkedin" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="LinkedIn">LinkedIn</a></li>--}}
                                {{--<li><a rel="nofollow" class="follow_instagram" href="http://t069.shop.ueeshop.com/"--}}
                                       {{--target="_blank" title="Instagram">Instagram</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="nav fr">
                    <dl>
                        <dt>Overview</dt>
                        <dd><a href="{{url('article/1')}}"
                               title="Location &amp; Working Hours">Location &amp; Working Hours</a></dd>
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/about-rockliffeocom_a0019.html"--}}
                        {{--title="About Rockliffeo.com">About Rockliffeo.com</a></dd>--}}
                        <dd><a href="{{url('article/2')}}" title="Our Guarantee">Our
                                Guarantee</a></dd>
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/location-amp-working-hours_a0007.html"--}}
                        {{--title="Location &amp; Working Hours">Location &amp; Working Hours</a></dd>--}}
                    </dl>
                    <dl>
                        <dt>Products &amp; Orders</dt>
                        <dd><a href="{{url('article/3')}}" title="Product Review">Product
                                Review</a></dd>
                        <dd><a href="{{url('article/4')}}"
                               title="Product Inquiry">Product Inquiry</a></dd>
                        <dd><a href="{{url('article/5')}}"
                               title="How to Place Order">How to Place Order</a></dd>
                        <dd><a href="{{url('article/6')}}"
                               title="24*7 Online Service">24*7 Online Service</a></dd>
                        {{--<dd><a href="http://t069.shop.ueeshop.com/art/color-chart_a0016.html" title="Color Chart">Color--}}
                        {{--Chart</a></dd>--}}
                    </dl>
                    {{--<dl>--}}
                    {{--<dt>Payment</dt>--}}
                    {{--<dd><a href="http://t069.shop.ueeshop.com/art/returing-for-repleacemrnt-and-refund_a0029.html"--}}
                    {{--title="Returing for Repleacemrnt and Refund">Returing for Repleacemrnt and Refund</a>--}}
                    {{--</dd>--}}
                    {{--<dd><a href="http://t069.shop.ueeshop.com/art/return-policy_a0028.html" title="Return Policy">Return--}}
                    {{--Policy</a></dd>--}}
                    {{--<dd><a href="http://t069.shop.ueeshop.com/art/paymet-terms_a0008.html" title="Paymet Terms">Paymet--}}
                    {{--Terms</a></dd>--}}
                    {{--</dl>--}}
                    <dl>
                        <dt>Help Center</dt>
                        <dd><a href="{{url('article/7')}}"
                               title="Conditions of Use">Conditions of Use</a>
                        </dd>
                    </dl>
                </div>
                <div class="clear"></div>
            </div>
            <div class="bot">
                <div class="copyright"> &nbsp;&nbsp;&nbsp;&nbsp; <a href="#" target="_blank">POWERED
                        BY WUXIAO</a></div>
            </div>
        </div>
    </div>
</div>
<div class="chattrans chatfloat4">
    <div id="service_3">
        <div class="sert"><img src="/Wedding Dress_files/bg4_0.png"></div>
        <div class="r r1 Color" style=" background-color:#FF7372;">
            {{--<a href="skype:3214879?chat" target="_blank" title="Mickey">Mickey</a>--}}
            <a href="#" target="_blank" title="Mickey">Mickey</a>
        </div>
        <div class="r r4 Color" style=" background-color:#FF7372;">
            <a href="javascript:void(0);" target="_blank" title="Lily">Lily</a>
            <span class="relimg"><img src="/"></span>
        </div>
        <div class="r r0 Color" style=" background-color:#FF7372;">
            {{--<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=123456789&amp;site=qq&amp;menu=yes" target="_blank" title="Lee">Lee</a>--}}
            <a href="#" target="_blank" title="Lee">Lee</a>
        </div>
        <div class="r top Color" style=" background-color:#FF7372;"><a
                    href="/">TOP</a></div>
    </div>
</div>

<script type="text/javascript">
    $('.chattrans').removeClass('chathide');
    $('#chat_float_btn').remove();
</script>
{{--<div align="center">--}}
    {{--<script type="text/javascript" src="/Wedding Dress_files/analytics.js"></script>--}}
    {{--<script src="/Wedding Dress_files/fixed.toper.js"></script>--}}
{{--</div>--}}
<iframe src="/Wedding Dress_files/source.html" style="display:none;"></iframe>
</body>
</html>