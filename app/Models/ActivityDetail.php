<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityDetail extends BaseModel
{
    protected $table = 'activity_detail';

    const CREATED_AT = 'add_time';
    const UPDATED_AT = 'upd_time';

    protected $dateFormat = 'U';

    protected $fillable = [
        'activity_id',
        'item_id'
    ];

    protected $casts = [
    ];

    public function activity(){
        return $this->belongsTo(Activity::class,'activity_id');
    }

    public function item(){
        return $this->belongsTo(AmzItem::class,'item_id');
    }
}
