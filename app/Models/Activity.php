<?php

namespace App\Models;

use App\Services\IStorageService;
use Illuminate\Database\Eloquent\Model;

class Activity extends BaseModel
{
    protected $table = 'activity';

    const CREATED_AT = 'add_time';
    const UPDATED_AT = 'upd_time';

    protected $dateFormat = 'U';

    protected $fillable = [
        'site_id',
        'name',
        'position',
        'image',
        'description',
        'creator_id',
    ];

    protected $casts = [
    ];

    public function site(){
        return $this->belongsTo(AmzSite::class,'site_id');
    }

    public function detail(){
        return $this->hasMany(ActivityDetail::class,'activity_id');
    }

    public function creator(){
        return $this->belongsTo(Creator::class,'creator_id');
    }

    /**
     * 商品数量
     * item_count
     */
    public function getItemCountAttribute()
    {
        return count($this->detail);
    }

    /**
     * 商品数量
     * item_list
     */
    public function getItemListAttribute()
    {
        return $this->detail->load(['item'])->map(function($item,$key){
            return $item->item;
        });
    }

    /**
     * 获取banner url绝对地址
     * image_url
     */
    public function getImageUrlAttribute()
    {
        if (empty($this->image)){
            return '';
//            return '/Wedding Dress_files/5dfc08f8af.png';
        }else{
            return resolve(IStorageService::class)->getUrl($this->image);
        }
    }
}
