<?php

namespace App\Models;

use App\Services\IStorageService;
use Illuminate\Database\Eloquent\Model;

class Creator extends BaseModel
{
    protected $table = 'creator';

    const CREATED_AT = 'add_time';
    const UPDATED_AT = 'upd_time';

    protected $dateFormat = 'U';

    protected $fillable = [
        'name',
        'image',
        'description',
    ];

    protected $casts = [
    ];

    /**
     * 获取banner url绝对地址
     * image_url
     */
    public function getImageUrlAttribute()
    {
        if (empty($this->image)){
            return '';
//            return '/Wedding Dress_files/5dfc08f8af.png';
        }else{
            return resolve(IStorageService::class)->getUrl($this->image);
        }
    }
}
