<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmzPage extends BaseModel
{

    protected $connection = 'amz';

    protected $table = 'amz_page';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    protected $fillable = [
    ];

    protected $casts = [
    ];

    /**
     * 标题
     * title
     */
    public function getTitleAttribute()
    {
        return $this->content['ItemAttributes']['Title'];
    }

    public function item()
    {
        return $this->belongsTo(AmzItem::class,'item_id');
    }
}
