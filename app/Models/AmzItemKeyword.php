<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmzItemKeyword extends BaseModel
{

    protected $connection = 'amz';

    protected $table = 'amz_item_keywords';

    public $timestamps = false;

    protected $fillable = [
    ];

    protected $casts = [
    ];

    public function item(){
        return $this->belongsTo(AmzItem::class,'item_id');
    }

    public function keyword(){
        return $this->belongsTo(AmzKeyword::class,'keyword_id');
    }
}
