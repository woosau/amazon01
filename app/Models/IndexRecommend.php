<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IndexRecommend extends BaseModel
{
    protected $table = 'index_recommend';

    const CREATED_AT = 'add_time';
    const UPDATED_AT = 'upd_time';

    protected $dateFormat = 'U';

    protected $fillable = [
        'item_id'
    ];

    protected $casts = [
    ];

    public function item(){
        return $this->belongsTo(AmzItem::class,'item_id');
    }
}
