<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmzItem extends BaseModel
{

    protected $connection = 'amz';

    protected $table = 'amz_item';

    public $timestamps = false;

    protected $fillable = [
    ];

    protected $casts = [
        'content'=>'array'
    ];

    public function keywords(){
        return $this->belongsToMany(AmzKeyword::class,AmzItemKeyword::table(),'item_id','keyword_id');
    }

    /**
     * seo标题
     * seo_title
     */
    public function getSeoTitleAttribute()
    {
        $title = $this->content['ItemAttributes']['Title'];
        $title = preg_replace("/[[:punct:]]/","",$title);
        $title = str_replace(" ","-",$title);
        $title = preg_replace("/-+/","-",$title);
        return trim($title,'-');
    }

    /**
     * 标题
     * title
     */
    public function getTitleAttribute()
    {
        return @$this->content['ItemAttributes']['Title'];
    }

    /**
     * 描述
     * description
     */
    public function getDescriptionAttribute()
    {
        return @$this->content['EditorialReviews']['EditorialReview']['Content'];
    }

    /**
     * 链接
     * url
     */
    public function getUrlAttribute()
    {
//        return @str_replace(["zapposkey01-20","wuxiao-20"],"shaoya01-20",$this->content['DetailPageURL']);
        return @str_replace(["wuxiao-20","shaoya01-20"],"zapposkey01-20",$this->content['DetailPageURL']);
    }

    /**
     * 图片链接
     * image_url
     */
    public function getImageUrlAttribute()
    {
        if (!empty($this->content['LargeImage']['URL'])){
            return $this->content['LargeImage']['URL'];
        }else{
            return 'http://p07ynlvj6.bkt.clouddn.com/no-image-available.jpg';
        }

    }

    /**
     * 图片列表
     * images_large
     */
    public function getImagesLargeAttribute()
    {
        $images = [];
        if (!empty($this->content['ImageSets']['ImageSet'])){
            foreach ($this->content['ImageSets']['ImageSet'] as $image){
                $images[] = $image['LargeImage']['URL'];
            }
        }
        return $images;
    }

    /**
     * 图片列表
     * images_small
     */
    public function getImagesSmallAttribute()
    {
        $images = [];
        if (!empty($this->content['ImageSets']['ImageSet'])){
            foreach ($this->content['ImageSets']['ImageSet'] as $image){
                $images[] = $image['TinyImage']['URL'];
            }
        }
        return $images;
    }

    /**
     * 价格文字
     * price_text
     */
    public function getPriceTextAttribute()
    {
        if (!empty($this->content['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'])){
            return $this->content['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'];
        }else if (!empty($this->content['OfferSummary']['LowestNewPrice']['FormattedPrice'])){
            return $this->content['OfferSummary']['LowestNewPrice']['FormattedPrice'];
        }else if (!empty($this->content['ItemAttributes']['ListPrice']['FormattedPrice'])){
            return $this->content['ItemAttributes']['ListPrice']['FormattedPrice'];
        }else if (!empty($this->content['OfferSummary']['LowestUsedPrice']['FormattedPrice'])){
            return $this->content['OfferSummary']['LowestUsedPrice']['FormattedPrice'];
        }
    }
}
