<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmzSite extends BaseModel
{

    protected $connection = 'amz';

    protected $table = 'amz_site';

    public $timestamps = false;

    protected $fillable = [
    ];

    protected $casts = [
    ];

    public function keywords(){
        return $this->hasMany(AmzKeyword::class,'site_id');
    }

    public function items(){
        return $this->hasManyThrough(AmzItemKeyword::class,AmzKeyword::class,'site_id','keyword_id');
    }

    public function pages(){
        return $this->hasMany(AmzPage::class,'site_id');
    }

//    public function publishedItems(){
//        return $this->hasManyThrough(AmzItem::class,AmzPage::class,'site_id','keyword_id');
//    }

    /**
     * 所有商品
     * item_list
     */
    public function getItemListAttribute()
    {
        return $this->items->map(function($item,$key){
            return $item->item;
        });
    }

    /**
     * 所有商品
     * item_list_limit
     */
    public function getItemListLimitAttribute()
    {
        return $this->items->take(65)->map(function($item,$key){
            return $item->item;
        });
    }

    /**
     * 所有已发布商品
     * published_item_list
     */
    public function getPublishedItemListAttribute()
    {
        return $this->pages->filter(function ($value, $key) {
            return $value->created <= date('Y-m-d');
        })->load(['item'])->map(function ($item, $key) {
            return $item->item;
        })->load(['keywords'])->values();
    }
}
