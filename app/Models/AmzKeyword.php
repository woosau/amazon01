<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmzKeyword extends BaseModel
{

    protected $connection = 'amz';

    protected $table = 'amz_keyword';

    public $timestamps = false;

    protected $fillable = [
    ];

    protected $casts = [
    ];

    public function keywords(){
        return $this->hasMany(AmzKeyword::class,'site_id');
    }

    public function items(){
        return $this->belongsToMany(AmzItem::class,AmzItemKeyword::table(),'keyword_id','item_id');
    }
}
