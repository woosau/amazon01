<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-20
 * Time: 17:42
 */

namespace App\Services;

/**
 *  前端构件
 * Interface ICouponService
 * @package App\Services
 */
interface IHtmlService
{

    /**
     * 单个头像上传功能区块，整个页面只能有一个此区块
     * @param string $name 文件表单name, 即在form中的表单名
     * @param string $value 文件表单默认值
     */
    public function logoUploader($name, $value = '');

    /**
     * 单个图片上传功能区块，页面可以有多个此区块
     * @param string $name 文件表单name, 即在form中的表单名
     * @param string $value 文件表单默认值
     */
    public function singleUploader($name,$value = '');

    /**
     * 多图上传功能区块
     * @param string $name 文件表单name, 即在form中的表单名
     * @param array $value 文件表单多图默认值
     */
    public function mutilUploader($name,$values = array());

}