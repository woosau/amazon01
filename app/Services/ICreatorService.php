<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:02
 */

namespace App\Services;

/**
 * 红人相关
 * Interface IMerchantService
 * @package App\Services
 */
interface ICreatorService
{
    /**
     * 列表
     * @param array $wheres
     * @return mixed
     */
    public function getList(array $wheres = []);

    /**
     * 所有列表
     * @param array $wheres
     * @return mixed
     */
    public function all(array $wheres = []);

    /**
     * 添加创作者
     * @param array $wheres
     * @return mixed
     */
    public function add($name,$image,$description);

    /**
     * 修改创作者
     * @param array $wheres
     * @return mixed
     */
    public function edit($creator_id,$name,$image,$description);

}