<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:02
 */

namespace App\Services;

/**
 * 产品相关
 * Interface IMerchantService
 * @package App\Services
 */
interface IProductService
{

    /**
     * 列表
     * @param array $wheres
     * @return mixed
     */
    public function all(array $wheres = []);

    /**
     * 所有
     * @param array $wheres
     * @return mixed
     */
    public function whole();

}