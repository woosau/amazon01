<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:02
 */

namespace App\Services;

/**
 * 推荐相关
 * Interface IMerchantService
 * @package App\Services
 */
interface IRecommendService
{

    /**
     * 首页推荐
     * @param array $wheres
     * @return mixed
     */
    public function index();

    /**
     * 首页推荐
     * @param array $wheres
     * @return mixed
     */
    public function editIndex(array $item_ids);

}