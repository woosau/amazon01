<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:02
 */

namespace App\Services;

/**
 * amz相关
 * Interface IMerchantService
 * @package App\Services
 */
interface IAmzService
{

    /**
     * 获取所有站点
     * @param $site_domain
     * @return mixed
     */
    public function getAllSites();

    /**
     * 获取站点
     * @param $site_domain
     * @return mixed
     */
    public function getSiteById($site_id);

    /**
     * 获取站点
     * @param $site_domain
     * @return mixed
     */
    public function getSiteByDomain($site_domain);

    /**
     * 获取第一个站点
     * @param $site_domain
     * @return mixed
     */
    public function getFirst();

    /**
     * 站点下商品列表
     * @param array $wheres
     * @return mixed
     */
    public function getItems($site_domain);

    /**
     * 站点下预约已发布商品
     * @param $site_domain
     * @return mixed
     */
    public function getPublishedItems($site_domain,array $wheres = []);

}