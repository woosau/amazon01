<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:04
 */

namespace App\Services\Impl;


use App\Models\AmzItem;
use App\Services\IProductService;
use DB;
use Log;

class ProductServiceImpl implements IProductService
{
    public function all(array $wheres = [])
    {
        $model = AmzItem::alias('t1')->select('t1.*')->orderBy('t1.created','DESC');
        if ($wheres){
            foreach ($wheres as $where){
                $model->where([$where]);
            }
        }
        return $model->paginate(config('common.perpage'));
    }

    public function whole()
    {
        return AmzItem::alias('t1')->select('t1.*')->orderBy('t1.created','DESC')->get();
    }
}