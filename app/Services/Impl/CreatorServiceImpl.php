<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:04
 */

namespace App\Services\Impl;


use App\Models\Creator;
use App\Services\ICreatorService;

class CreatorServiceImpl implements ICreatorService
{

    public function getList(array $wheres = [])
    {
        $model = Creator::alias('t1')->select('t1.*')->orderBy('t1.add_time','DESC');
        if ($wheres){
            foreach ($wheres as $where){
                $model->where([$where]);
            }
        }
        return $model->paginate(config('common.perpage'));
    }

    public function all(array $wheres = [])
    {
        $model = Creator::alias('t1')->select('t1.*')->orderBy('t1.add_time','DESC');
        if ($wheres){
            foreach ($wheres as $where){
                $model->where([$where]);
            }
        }
        return $model->get();
    }

    public function add($name,$image,$description)
    {
        $creator = Creator::create([
            'name'=>$name,
            'image'=>$image,
            'description'=>$description,
        ]);
        if (!$creator){
            return false;
        }
        return true;
    }

    public function edit($creator_id,$name,$image,$description)
    {
        $creator = Creator::find($creator_id);
        if (!$creator){
            return false;
        }

        $ret = $creator->update([
            'name'=>$name,
            'image'=>$image,
            'description'=>$description,
        ]);
        if (!$ret){
            return false;
        }
        return true;
    }
}