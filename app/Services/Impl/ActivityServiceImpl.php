<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:04
 */

namespace App\Services\Impl;


use App\Models\Activity;
use App\Models\ActivityDetail;
use App\Models\AmzItem;
use App\Services\IActivityService;
use App\Services\IProductService;
use DB;
use Log;

class ActivityServiceImpl implements IActivityService
{
    public function all(array $wheres = [])
    {
        $model = Activity::alias('t1')->select('t1.*')->orderBy('t1.add_time','DESC');
        if ($wheres){
            foreach ($wheres as $where){
                if ($where[0] == 'asin'){
                    $model->join(ActivityDetail::table().' as ad', function ($join) use ($where){
                        $join->on('t1.id', '=', 'ad.activity_id');
                    })->join(AmzItem::table().' as i', function ($join) use ($where){
                        $join->on('ad.item_id', '=', 'i.id');
                    });
                }
                $model->where([$where]);
            }
        }
        return $model->paginate(config('common.perpage'));
    }

    public function listBySiteId($site_id, array $wheres = [])
    {
        $model = Activity::alias('t1')->select('t1.*')->orderBy('t1.add_time','DESC');
        $model->where('site_id',$site_id);
        if ($wheres){
            foreach ($wheres as $where){
                if ($where[0] == 'asin'){
                    $model->join(ActivityDetail::table().' as ad', function ($join) use ($where){
                        $join->on('t1.id', '=', 'ad.activity_id');
                    })->join(AmzItem::table().' as i', function ($join) use ($where){
                        $join->on('ad.item_id', '=', 'i.id');
                    });
                }
                $model->where([$where]);
            }
        }
        return $model->paginate(config('common.perpage'));
    }

    public function getById($activity_id)
    {
        return Activity::find($activity_id);
    }

    public function getByName($activity_name)
    {
        return Activity::where('name',$activity_name)->first();
    }

    public function add($site_id,$name,$position,$image, $description,$creator_id,array $item_ids)
    {
        $activity = Activity::create([
            'site_id'=>$site_id,
            'name'=>$name,
            'position'=>$position,
            'image'=>$image,
            'description'=>$description,
            'creator_id'=>$creator_id,
        ]);
        if (!$activity){
            return false;
        }

//        ActivityDetail::where('activity_id', $activity->id)->delete();
        foreach ($item_ids as $item_id){
            ActivityDetail::create([
                'activity_id'=>$activity->id,
                'item_id'=>$item_id,
            ]);
        }
        return true;
    }

    public function edit($activity_id, $name,$position,$image,$description, $creator_id,array $item_ids)
    {
        $activity = Activity::find($activity_id);
        if (!$activity){
            return false;
        }

        $ret = $activity->update([
            'name'=>$name,
            'position'=>$position,
            'image'=>$image,
            'description'=>$description,
            'creator_id'=>$creator_id,
        ]);
        if (!$ret){
            return false;
        }

        ActivityDetail::where('activity_id', $activity_id)->delete();
        foreach ($item_ids as $item_id){
            ActivityDetail::create([
                'activity_id'=>$activity_id,
                'item_id'=>$item_id,
            ]);
        }
        return true;
    }

    public function addItem($activity_id, $item_id)
    {
        $item = ActivityDetail::where('activity_id',$activity_id)->where('item_id',$item_id)->first();
        if ($item){
            return false;
        }
        return ActivityDetail::create([
            'activity_id'=>$activity_id,
            'item_id'=>$item_id,
        ]);
    }
}