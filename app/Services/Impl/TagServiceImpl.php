<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:04
 */

namespace App\Services\Impl;


use App\Models\Activity;
use App\Models\ActivityDetail;
use App\Models\AmzItem;
use App\Models\AmzKeyword;
use App\Services\IActivityService;
use App\Services\IProductService;
use App\Services\ITagService;
use DB;
use Log;

class TagServiceImpl implements ITagService
{

    public function getById($keyword_id)
    {
        return AmzKeyword::find($keyword_id);
    }

    public function getByWord($keyword)
    {
        return AmzKeyword::where('word',$keyword)->first();
    }
}