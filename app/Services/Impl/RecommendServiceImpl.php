<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:04
 */

namespace App\Services\Impl;


use App\Models\Activity;
use App\Models\ActivityDetail;
use App\Models\IndexRecommend;
use App\Services\IActivityService;
use App\Services\IProductService;
use App\Services\IRecommendService;
use DB;
use Log;

class RecommendServiceImpl implements IRecommendService
{
    public function index()
    {
        $model = IndexRecommend::alias('t1')->select('t1.*')->orderBy('t1.add_time','DESC')->get()->map(function ($item, $key) {
            return $item->item;
        });
        return $model;
    }

    public function editIndex(array $item_ids)
    {
        IndexRecommend::where('id','>',1)->delete();
        foreach ($item_ids as $item_id){
            IndexRecommend::create([
                'item_id'=>$item_id,
            ]);
        }
        return true;
    }
}