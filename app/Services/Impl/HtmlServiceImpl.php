<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-20
 * Time: 17:42
 */

namespace App\Services\Impl;


use App\Services\IHtmlService;
use Auth;
use DB;
use Log;

class HtmlServiceImpl implements IHtmlService
{
    public function logoUploader($name, $value = '')
    {
        return view('uploader/logo',[
            'name'=>$name,
            'value'=>$value,
        ]);
    }

    public function singleUploader($name, $value = '')
    {
        static $hadRender = false;
        if (!$hadRender){
            $url = '/js';
            $pre = <<<EOT
<link href="$url/webuploader/webuploader.css" rel="stylesheet" type="text/css" media="screen" />
<link href="$url/webuploader/upload.css" rel="stylesheet" type="text/css" media="screen" />
<script src="$url/webuploader/webuploader.min.js" charset="UTF-8" type="text/javascript"></script>
<script>
    var uploaders = [], uidx = 0;
</script>
EOT;
            $hadRender = true;
        }else{
            $pre = '';
        }

        $id = sprintf('%u',crc32(uniqid(true)));

        return view('uploader/single',[
            'pre'=>$pre,
            'id'=>$id,
            'name'=>$name,
            'value'=>$value,
        ]);
    }

    public function mutilUploader($name, $values = array())
    {
        // TODO: Implement mutil() method.
    }


}