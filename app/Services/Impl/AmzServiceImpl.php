<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:04
 */

namespace App\Services\Impl;


use App\Models\Activity;
use App\Models\ActivityDetail;
use App\Models\AmzItem;
use App\Models\AmzItemKeyword;
use App\Models\AmzKeyword;
use App\Models\AmzPage;
use App\Models\AmzSite;
use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\IProductService;
use DB;
use Log;

class AmzServiceImpl implements IAmzService
{
    public function getAllSites()
    {
        return AmzSite::all();
    }

    public function getSiteById($site_id)
    {
        return AmzSite::find($site_id);
    }

    public function getSiteByDomain($site_domain)
    {
        return AmzSite::where('domain',$site_domain)->first();
    }

    public function getFirst()
    {
        return AmzSite::first();
    }

    public function getItems($site_domain)
    {
        $site = AmzSite::where('domain',$site_domain)->first();
        if (!$site){
            return;
        }
//        return $site->items;
//        return $site->item_list;
        return $site->item_list_limit;
    }

    public function getPublishedItems($site_domain,array $wheres = [])
    {
        $site = AmzSite::where('domain',$site_domain)->first();
        if (!$site){
            return;
        }
//        $keyword_ids = AmzKeyword::where('site_id',$site->id)->pluck('id');
//        $model = AmzItem::alias('t1')->select('t1.*')->groupBy("t1.id")->join(AmzItemKeyword::table().' as ak', function ($join) use ($keyword_ids){
//            $join->on('t1.id', '=', 'ak.item_id')->whereIn('ak.keyword_id',$keyword_ids);
//        });
        $model = AmzItem::alias('t1')->select('t1.*')->groupBy("t1.id")->join(AmzPage::table().' as ap', function ($join) use ($site){
            $join->on('t1.id', '=', 'ap.item_id')->where('ap.site_id',$site->id);
        });
        $model->where('ap.created','<=',date('Y-m-d'));
        if ($wheres){
            foreach ($wheres as $where){
                $model->where([$where]);
            }
        }
        return $model->paginate(config('common.perpage'));
//        return $site->published_item_list;
    }
}