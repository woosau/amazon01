<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:02
 */

namespace App\Services;

/**
 * 产品相关
 * Interface IMerchantService
 * @package App\Services
 */
interface ITagService
{
    /**
     * 取关键词
     * @param array $wheres
     * @return mixed
     */
    public function getById($keyword_id);

    /**
     * 取关键词
     * @param array $wheres
     * @return mixed
     */
    public function getByWord($keyword);

}