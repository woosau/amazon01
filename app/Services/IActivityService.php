<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-04-03
 * Time: 1:02
 */

namespace App\Services;

/**
 * 产品相关
 * Interface IMerchantService
 * @package App\Services
 */
interface IActivityService
{

    /**
     * 列表
     * @param array $wheres
     * @return mixed
     */
    public function all(array $wheres = []);

    /**
     * 根据站点区分列表
     * @param array $wheres
     * @return mixed
     */
    public function listBySiteId($site_id,array $wheres = []);

    /**
     * 取集合
     * @param array $wheres
     * @return mixed
     */
    public function getById($activity_id);

    /**
     * 取集合
     * @param array $wheres
     * @return mixed
     */
    public function getByName($activity_name);

    /**
     * 添加活动
     * @param array $wheres
     * @return mixed
     */
    public function add($site_id,$name,$position,$image,$description,$creator_id,array $item_ids);

    /**
     * 修改活动
     * @param array $wheres
     * @return mixed
     */
    public function edit($activity_id,$name,$position,$image,$description,$creator_id,array $item_ids);

    /**
     * 集合添加一个商品
     * @param $activity_id
     * @param $item_id
     * @return mixed
     */
    public function addItem($activity_id,$item_id);

}