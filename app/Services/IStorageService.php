<?php
/**
 * Created by PhpStorm.
 * User: ownfi
 * Date: 2017-03-22
 * Time: 12:34
 */

namespace App\Services;

/**
 * 存储相关
 * Interface IStorageService
 * @package App\Services
 */
interface IStorageService
{

    /**
     * 保存上传文件
     * @param $key
     * @return mixed
     */
    public function saveUpload($key);

    /**
     * 保存本地文件
     * @param $path_path
     * @return mixed
     */
    public function saveFile($path);

    /**
     * 保存二进制文件流
     * @param $binary_stream
     * @return mixed
     */
    public function saveStream($binary_stream);

    /**
     * 抓取在线文件保存
     * @param $url
     * @return mixed
     */
    public function saveUrl($url);

    /**
     * 获取文件本地绝对路径
     * @param $path
     * @return mixed
     */
    public function getRealPath($path);

    /**
     * 获取文件url绝对地址
     * @param $path
     * @return mixed
     */
    public function getUrl($path);

    /**
     * 获取文件uri相对路径
     * @param $path
     * @return mixed
     */
    public function getUri($path);

    /**
     * 检查文件是否存在
     * @param $path
     * @return mixed
     */
    public function fileExists($path);

    /**
     * 提取文件内容
     * @param $path
     * @return mixed
     */
    public function getContent($path);

    /**
     * 获取文件输出响应
     * @param $path
     * @return mixed
     */
    public function getResponse($path);

}