<?php

namespace App\Http\Middleware;

use App\Services\IAmzService;
use Closure;
use Log;
use URL;
use View;

/**
 * 后台
 * @package App\Http\Middleware
 */
class Backend
{

    public function __construct()
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $amzService = resolve(IAmzService::class);

        $domain = parse_url(URL::current(),PHP_URL_HOST);
        if (!$site = session(BACKSITE)){
            $site = $amzService->getSiteByDomain($domain);
            if (empty($site)){
                $site = $amzService->getSiteByDomain('www.amz001.com');
            }
            if (empty($site)){
                $site = $amzService->getFirst();
            }
            session([BACKSITE=>$site]);
        }

        View::share('sites',$amzService->getAllSites());

        return $next($request);
    }
}
