<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use View;

/**
 * 自动重定向记录
 * @package App\Http\Middleware
 */
class RedirectUrl
{

    public function __construct()
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->ajax()){
            if ($request->has('redirectUrl')){
                View::share('_refer',$request->input('redirectUrl'));
            }else{
                View::share('_refer',$request->header('referer'));
            }
        }

        return $next($request);
    }
}
