<?php

namespace App\Http\Controllers;

use App\Services\IMasterService;
use App\Services\ISystemService;
use DB;
use Illuminate\Http\Request;
use Schema;

/**
 * 测试控制器
 */
class TestController extends BaseController
{

    /**
     * @param Request $request
     * @param ISystemService $systemService
     * @param IMasterService $masterService
     * @return string
     * @throws \Exception
     */
    public function index(Request $request)
    {
        return 'this is test';
    }

    public function phpinfo()
    {
        phpinfo();
    }

}
