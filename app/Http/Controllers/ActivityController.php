<?php

namespace App\Http\Controllers;
use App\Libs\Filter\Filter;
use App\Models\Activity;
use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\IHtmlService;
use App\Services\IProductService;
use Illuminate\Http\Request;

class ActivityController extends BaseController
{
    /**
     * 首页
     * @param Request $request
     */
    public function all(Request $request,$activity_name,IActivityService $activityService,IAmzService $amzService)
    {
        $site = $request->session()->get(FRONTSITE);
        $activity_list = $activityService->listBySiteId($site->id);
        $activity = $activityService->getByName($activity_name);
        $list = $activity->item_list;

        $list_title = $activity->name;
        $list_description = $activity->description;
        return view('index/index',['activity_list'=>[],
            'list_title'=>$list_title,'list_description'=>$list_description,
            'list'=>$list,'collections_list'=>$activity_list]);
    }
}
