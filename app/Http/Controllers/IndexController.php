<?php

namespace App\Http\Controllers;

use App\Models\AmzItem;
use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\IRecommendService;
use Illuminate\Http\Request;

class IndexController extends BaseController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,IAmzService $amzService,IActivityService $activityService)
    {
        $site = $request->session()->get(FRONTSITE);
        $activity_list = $activityService->listBySiteId($site->id);
        $list = $amzService->getPublishedItems($site->domain);
        $list_title = 'What\'s Hot';

        $random_key = sprintf('%u',crc32($site->domain));

        return view('index/index',['activity_list'=>$activity_list,'list_title'=>$list_title,'list'=>$list,'random_key'=>$random_key,'collections_list'=>$activity_list]);
    }

    public function product($asin,$seo_title,Request $request,IAmzService $amzService,IActivityService $activityService){
        $site = $request->session()->get(FRONTSITE);
        $activity_list = $activityService->listBySiteId($site->id);
        $product = AmzItem::where('asin',$asin)->first();
//        dump($item->images_small);
        return view('product/index',['product'=>$product,'activity_list'=>$activity_list,'collections_list'=>$activity_list]);
    }
}
