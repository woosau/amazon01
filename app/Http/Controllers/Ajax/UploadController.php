<?php

namespace App\Http\Controllers\Ajax;

use App\Common\Constant\Status;
use App\Services\IStorageService;

/**
 * 上传
 * Class MerchantController
 * @package App\Http\Controllers
 */
class UploadController extends BaseController
{
    /**
     * 图片上传方法
     */
    public function uploadimg(IStorageService $storageService)
    {
        $images = $storageService->saveUpload('images');
        if ($images)
        {
            return $this->renderJson(Status::SUCCESS, array(
                'uri' => $images,
                'url' => $storageService->getUrl($images)
            ), '上传成功');
        }else
        {
            header('HTTP/1.1 500 Server Error');
            return $this->renderJson(Status::ERROR, $images, '上传失败');
        }
    }

}
