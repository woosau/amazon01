<?php

namespace App\Http\Controllers\Admin;
use App\Libs\Filter\Filter;
use App\Models\Activity;
use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\IHtmlService;
use App\Services\IProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{

    /**
     * 切换站点
     * @param Request $request
     */
    public function change(Request $request,$site_id,IAmzService $amzService)
    {
        $site = $amzService->getSiteById($site_id);
        if (!empty($site)){
            session([BACKSITE=>$site]);
        }
        return back();
    }
}
