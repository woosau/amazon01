<?php

namespace App\Http\Controllers\Admin;
use App\Services\IProductService;
use App\Services\IRecommendService;
use Illuminate\Http\Request;

class RecommendController extends BaseController
{

    /**
     * 编辑
     * @param Request $request
     */
    public function index(Request $request,IProductService $productService,IRecommendService $recommendService)
    {
        if ($request->isMethod('post')){
            $recommendService->editIndex($request->input('item_id'));
            return back();
        }

        $products = $productService->whole();
        $items = $recommendService->index();
        return view('admin/recommend/index',['products'=>$products,'items'=>$items]);
    }
}
