<?php

namespace App\Http\Controllers\Admin;
use App\Libs\Filter\Filter;
use App\Models\Activity;
use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\ICreatorService;
use App\Services\IHtmlService;
use App\Services\IProductService;
use Illuminate\Http\Request;

class ActivityController extends BaseController
{
    /**
     * 首页
     * @param Request $request
     */
    public function all(Request $request,IActivityService $activityService,IAmzService $amzService)
    {
        $site = $request->session()->get(BACKSITE);

        $filter = Filter::instance()
            ->HTML('ASIN：')
            ->input('asin')
//            ->HTML('手机号：')
//            ->input('me_phone')
//            ->HTML('用户类型：')
//            ->select('me_category',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('下单情况：')
//            ->select('orders',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->br()->br()
//            ->HTML('定价下单：')
//            ->select('merchant_status',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('账户封号：')
//            ->select('me_status',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('注册时间：')
//            ->date('me_add_time')
            ->br()->br()
            ->reset()->submit();
        if ($filter->is_filtering()){
            $list = $activityService->listBySiteId($site->id,$filter->get_where());
        }else{
            $list = $activityService->listBySiteId($site->id);
        }

        return view('admin/activity/all',['list'=>$list,'filter'=>$filter]);
    }

    /**
     * 添加
     * @param Request $request
     */
    public function add(Request $request, ICreatorService $createrService, IActivityService $activityService, IHtmlService $htmlService, IAmzService $amzService)
    {
        $site = $amzService->getSiteByDomain(session(BACKSITE)->domain);

        if ($request->isMethod('post')){
            $activityService->add($site->id,$request->input('name'),$request->input('position'),$request->input('image'),$request->input('description'),$request->input('creator_id'),$request->input('item_id',[]));
            return redirect('/admin/activity/all');
        }

//        $products = $amzService->getItems($site->domain);
        $products = [];
        $creators = $createrService->all();
        $uploader = $htmlService->singleUploader('image');
        return view('admin/activity/add',[
            'products'=>$products,
            'creators'=>$creators,
            'uploader'=>$uploader,
            'site'=>$site
        ]);
    }

    /**
     * 编辑
     * @param Request $request
     */
    public function edit(Request $request, Activity $activity, ICreatorService $createrService, IActivityService $activityService, IHtmlService $htmlService, IAmzService $amzService)
    {
        $site = $activity->site;

        if ($request->isMethod('post')){
            $activityService->edit($activity->id,$request->input('name'),$request->input('position'),$request->input('image'),$request->input('description'),$request->input('creator_id'),$request->input('item_id',[]));
            return redirect('/admin/activity/all');
        }

//        $products = $amzService->getItems($site->domain);
        $products = [];
        $creators = $createrService->all();
        $uploader = $htmlService->singleUploader('image',$activity->image);
        return view('admin/activity/edit',[
            'products'=>$products,
            'creators'=>$creators,
            'activity'=>$activity,
            'uploader'=>$uploader,
            'site'=>$site
        ]);
    }

    /**
     * 删除
     * @param Request $request
     */
    public function del(Request $request,Activity $activity)
    {
        foreach ($activity->detail as $detail){
            $detail->delete();
        }
        $activity->delete();
        return back();
    }
}
