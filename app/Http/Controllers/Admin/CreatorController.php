<?php

namespace App\Http\Controllers\Admin;
use App\Libs\Filter\Filter;
use App\Models\Activity;
use App\Models\Creator;
use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\ICreatorService;
use App\Services\IHtmlService;
use App\Services\IProductService;
use Illuminate\Http\Request;

class CreatorController extends BaseController
{
    /**
     * 首页
     * @param Request $request
     */
    public function all(Request $request,ICreatorService $creatorService,IAmzService $amzService)
    {
        $site = $request->session()->get(BACKSITE);

        $filter = Filter::instance()
            ->HTML('名称：')
            ->input('name')
//            ->HTML('手机号：')
//            ->input('me_phone')
//            ->HTML('用户类型：')
//            ->select('me_category',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('下单情况：')
//            ->select('orders',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->br()->br()
//            ->HTML('定价下单：')
//            ->select('merchant_status',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('账户封号：')
//            ->select('me_status',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('注册时间：')
//            ->date('me_add_time')
            ->br()->br()
            ->reset()->submit();
        if ($filter->is_filtering()){
            $list = $creatorService->getList($filter->get_where());
        }else{
            $list = $creatorService->getList();
        }

        return view('admin/creator/all',['list'=>$list,'filter'=>$filter]);
    }

    /**
     * 添加
     * @param Request $request
     */
    public function add(Request $request, ICreatorService $creatorService, IHtmlService $htmlService)
    {
        if ($request->isMethod('post')){
            $creatorService->add($request->input('name'),$request->input('image'),$request->input('description'));
            return redirect('/admin/creator/all');
        }

        $uploader = $htmlService->singleUploader('image');
        return view('admin/creator/add',[
            'uploader'=>$uploader,
        ]);
    }

    /**
     * 编辑
     * @param Request $request
     */
    public function edit(Request $request, Creator $creator, ICreatorService $createrService, IActivityService $activityService, IHtmlService $htmlService, IAmzService $amzService)
    {
        if ($request->isMethod('post')){
            $createrService->edit($creator->id,$request->input('name'),$request->input('image'),$request->input('description'));
            return redirect('/admin/creator/all');
        }

        $uploader = $htmlService->singleUploader('image',$creator->image);
        return view('admin/creator/edit',[
            'creator'=>$creator,
            'uploader'=>$uploader,
        ]);
    }

    /**
     * 删除
     * @param Request $request
     */
    public function del(Request $request,Creator $creator)
    {
        $creator->delete();
        return back();
    }
}
