<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

/**
 * 页面样例
 * Class DemoController
 * @package App\Http\Controllers
 */
class DemoController extends BaseController
{
    /**
     * 首页
     * @param Request $request
     */
    public function index(Request $request){

        return view('admin/demo/index',[]);
    }

    public function form(Request $request){

        return view('admin/demo/form',[]);
    }

    public function table(Request $request){

        return view('admin/demo/table',[]);
    }

    public function button(Request $request){

        return view('admin/demo/button',[]);
    }
}
