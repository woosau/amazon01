<?php

namespace App\Http\Controllers\Admin;
use App\Libs\Filter\Filter;
use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\IProductService;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    /**
     * 首页
     * @param Request $request
     */
    public function index(Request $request,IProductService $productService,IAmzService $amzService,IActivityService $activityService)
    {
        $site = $request->session()->get(BACKSITE);

        $filter = Filter::instance()
            ->HTML('ASIN：')
            ->input('asin')
            ->HTML('标题/内容：')
            ->input('content')
            ->br()->br()
            ->reset()->submit();;
        if ($filter->is_filtering()){
            $list = $amzService->getPublishedItems(session(BACKSITE)->domain,$filter->get_where());
            $list = $productService->all($filter->get_where());
        }else{
            $list = $amzService->getPublishedItems(session(BACKSITE)->domain);
        }
//        dump($list);exit;

        $activity_list = $activityService->listBySiteId($site->id);
        return view('admin/product/index',['list'=>$list,'filter'=>$filter,'activity_list'=>$activity_list]);
    }

    /**
     * 首页
     * @param Request $request
     */
    public function all(Request $request,IProductService $productService)
    {
        $filter = Filter::instance()
            ->HTML('ASIN：')
            ->input('asin')
//            ->HTML('手机号：')
//            ->input('me_phone')
//            ->HTML('用户类型：')
//            ->select('me_category',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('下单情况：')
//            ->select('orders',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->br()->br()
//            ->HTML('定价下单：')
//            ->select('merchant_status',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('账户封号：')
//            ->select('me_status',array(''=>'请选择'),null,array('style'=>'width:150px'))
//            ->HTML('注册时间：')
//            ->date('me_add_time')
            ->br()->br()
            ->reset()->submit();
        if ($filter->is_filtering()){
            $list = $productService->all($filter->get_where());
        }else{
            $list = $productService->all();
        }

        return view('admin/product/all',['list'=>$list,'filter'=>$filter]);
    }

    /**
     * 添加商品到集合
     * @param Request $request
     * @param IProductService $productService
     */
    public function add2activity(Request $request,IActivityService $activityService){
        $ret = $activityService->addItem($request->input('activity_id'),$request->input('item_id'));
        if ($ret){
            return $this->renderJson(200);
        }else{
            return $this->renderJson(500,'','集合中已存在该商品');
        }
    }
}
