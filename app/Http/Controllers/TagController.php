<?php

namespace App\Http\Controllers;
use App\Libs\Filter\Filter;
use App\Models\Activity;
use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\IHtmlService;
use App\Services\IProductService;
use App\Services\ITagService;
use Illuminate\Http\Request;

class TagController extends BaseController
{
    /**
     * 首页
     * @param Request $request
     */
    public function all(Request $request,$keyword,IActivityService $activityService,ITagService $tagService,IAmzService $amzService)
    {
        $site = $request->session()->get(FRONTSITE);
        $activity_list = $activityService->listBySiteId($site->id);
        $keyword = $tagService->getByWord(urldecode($keyword));
        $list = $keyword->items;

        $list_title = '#'.$keyword->word;
        return view('index/index',['activity_list'=>[],'list_title'=>$list_title,'list'=>$list,'collections_list'=>$activity_list]);
    }
}
