<?php
use App\Common\Constant\Params;
use App\Common\Constant\Status;

require __DIR__.'/helpers.php';

function status($key){
    return !empty(Status::$__values[$key]) ? Status::$__values[$key] : null;
}