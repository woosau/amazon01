<?php
namespace App\Common\Constant;

/**
 * 状态码
 *
 * @author ownfi
 */
class Status {

    const SUCCESS = 200;

    const FAIL = 400;

    const ERROR = 500;

    /**
     * 系统相关
     */
    const SYSTEM = 10000;

    /**
     * 用户相关
     */
    const USER = 20000;

    /**
     * 会话相关
     */
    const SESSION = 30000;

    /**
     * 微信相关
     */
    const WEIXIN = 40000;

    /**
     * 手机相关
     */
    const MOBILE = 50000;

    /**
     * 优惠券相关
     */
    const COUPON_NOT_ENOUGH = 100001;

    static $__values = [
        self::SUCCESS=>'成功',
        self::FAIL=>'操作失败',
        self::ERROR=>'遇到错误',
    ];
}
