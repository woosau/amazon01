<?php

namespace App\Providers;

use App\Services\IActivityService;
use App\Services\IAmzService;
use App\Services\ICreatorService;
use App\Services\IHtmlService;
use App\Services\Impl\ActivityServiceImpl;
use App\Services\Impl\AmzServiceImpl;
use App\Services\Impl\CreatorServiceImpl;
use App\Services\Impl\HtmlServiceImpl;
use App\Services\Impl\ProductServiceImpl;
use App\Services\Impl\QiniuStorageServiceImpl;
use App\Services\Impl\RecommendServiceImpl;
use App\Services\Impl\TagServiceImpl;
use App\Services\IProductService;
use App\Services\IRecommendService;
use App\Services\IStorageService;
use App\Services\ITagService;
use Illuminate\Support\ServiceProvider;

class ProjectServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IProductService::class, ProductServiceImpl::class);
        $this->app->singleton(IActivityService::class, ActivityServiceImpl::class);
        $this->app->singleton(IRecommendService::class, RecommendServiceImpl::class);
        $this->app->singleton(IHtmlService::class, HtmlServiceImpl::class);
        $this->app->singleton(IStorageService::class, QiniuStorageServiceImpl::class);
        $this->app->singleton(IAmzService::class, AmzServiceImpl::class);
        $this->app->singleton(ITagService::class, TagServiceImpl::class);
        $this->app->singleton(ICreatorService::class, CreatorServiceImpl::class);
    }
}
