/*
Navicat MySQL Data Transfer

Source Server         : localhost_31306
Source Server Version : 50716
Source Host           : localhost:31306
Source Database       : amazon01

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2017-12-05 16:32:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL COMMENT '图片',
  `add_time` int(10) unsigned DEFAULT NULL,
  `upd_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES ('4', '2', 'dsdsds', null, null, '1512029290', '1512029290');
INSERT INTO `activity` VALUES ('5', '1', 'ggg', null, null, '1512457642', '1512457642');

-- ----------------------------
-- Table structure for activity_detail
-- ----------------------------
DROP TABLE IF EXISTS `activity_detail`;
CREATE TABLE `activity_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `add_time` int(10) unsigned DEFAULT NULL,
  `upd_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity_detail
-- ----------------------------
INSERT INTO `activity_detail` VALUES ('3', '2', '2', '1511773074', '1511773074');
INSERT INTO `activity_detail` VALUES ('5', '4', '2', '1512462563', '1512462563');
INSERT INTO `activity_detail` VALUES ('6', '4', '5', '1512462563', '1512462563');

-- ----------------------------
-- Table structure for index_recommend
-- ----------------------------
DROP TABLE IF EXISTS `index_recommend`;
CREATE TABLE `index_recommend` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `add_time` int(10) unsigned DEFAULT NULL,
  `upd_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of index_recommend
-- ----------------------------
INSERT INTO `index_recommend` VALUES ('1', '257', '1512356118', '1512356118');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', 'admin@admin.com', '$2y$10$wh/Byi9pW5lP3AVQUdfu8uutvbzmF6FAXBfdgWyZjruVM4dVoT0AO', '0wuvn0GhBKi6XeUOXKwVvwUcpAUINBro6auivUGIufFdb8TpGuB1ciuJTllI', '2017-11-24 07:17:00', '2017-11-24 07:17:00');
