<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['middleware'=>['frontend']], function(){
    Route::get('/', 'IndexController@index');
    Route::get('/product/{asin}/{seo_title}', 'IndexController@product');
    Route::get('/article/{article_id}', 'ArticleController@view');
    Route::get('/activity/{activity_name}', 'ActivityController@all');

    Route::get('/tag/{keyword}', 'TagController@all');
});

Route::group(['namespace'=>'Admin','prefix' => 'admin','middleware'=>['auth','backend']], function(){
    Route::get('/', 'IndexController@index')->name('/');

    Route::any('/changesite/{site_id}', 'SiteController@change');

    Route::get('/product/all', 'ProductController@all');
    Route::get('/product/index', 'ProductController@index');
    Route::post('/product/add2activity', 'ProductController@add2activity');
    Route::any('/recommend/index', 'RecommendController@index');

    Route::get('/activity/all', 'ActivityController@all');
    Route::any('/activity/add', 'ActivityController@add');
    Route::get('/activity/del/{activity}', 'ActivityController@del');
    Route::any('/activity/edit/{activity}', 'ActivityController@edit');

    Route::get('/creator/all', 'CreatorController@all');
    Route::any('/creator/add', 'CreatorController@add');
    Route::get('/creator/del/{creator}', 'CreatorController@del');
    Route::any('/creator/edit/{creator}', 'CreatorController@edit');

    Route::get('/demo', 'DemoController@index');
    Route::get('/demo/form', 'DemoController@form');
    Route::get('/demo/table', 'DemoController@table');
    Route::get('/demo/button', 'DemoController@button');
});

Route::group(['namespace'=>'Ajax','prefix' => 'ajax'], function(){
    Route::post('/upload/uploadimg', 'UploadController@uploadimg');
});

Route::any('/test', 'TestController@index');
Route::any('/phpinfo', 'TestController@phpinfo');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');