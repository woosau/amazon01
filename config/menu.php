<?php
return [
//    '后台首页'=>[
//        '运营概况'=>'/',
//        '订单概况'=>'statistic/order',
//        '商家概况'=>'statistic/merchant',
//        '师傅概况'=>'statistic/master',
//        '财务概况'=>'statistic/finance',
//    ],
    '后台首页'=>'/admin',
//    '站点列表'=>[
//        '所有站点'=>'admin/site/all',
//        '所有商品'=>'admin/product/all',
//    ],
    '商品查看'=>[
        '商品列表'=>'admin/product/index',
//        '首页推荐'=>'admin/recommend/index',
        '商品集合列表'=>'admin/activity/all',
        '创作者列表'=>'admin/creator/all',
    ],
//    '系统设置'=>[
//        '通知事件日志'=>'#',
//        '系统操作日志'=>'#'
//    ],
    'DEMO'=>[
        '报表'=>'admin/demo',
        '表单'=>'admin/demo/form',
        '列表'=>'admin/demo/table',
        '按钮'=>'admin/demo/button'
    ],
];