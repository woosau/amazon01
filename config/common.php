<?php

use App\Common\Constant\Params;

define('FRONTSITE','frontend_site');
define('BACKSITE','backend_site');

return [

    /**
     * 是否启用全站伪造数据
     */
    'mock'=>false,

    /**
     * 分页默认每页结果数
     */
    'perpage'=>10,

    /**
     * 是否验证api请求签名
     */
    'verifyApi'=>true,

    /**
     * 是否验证用户访问权限
     */
    'verifyAccess' => false,
];