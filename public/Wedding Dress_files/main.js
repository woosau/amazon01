/*
Powered by ly200.com		http://www.ly200.com
广州联雅网络科技有限公司		020-83226791
*/

$(document).ready(function(){	
	/****** 导航显示 Start ******/
	function navShow(){
		var $obj=$('#nav'),
			navItemWidth=0,
			navWidth=$obj.width();
		$obj.css('overflow', 'visible').find('.nav_item>li').each(function(){
			navItemWidth+=$(this).outerWidth();
			if(navItemWidth>navWidth){	//2为2像素差
				$(this).hide();
			}else{
				$(this).show();
			}
			//console.log(navItemWidth+'----'+navWidth);
		});
	}
	setTimeout(function(){navShow();},200);
	$(window).resize(function(){ navShow(); });
	var big_width=$('body').hasClass('w_1200');
	$('#nav .nav_item li').each(function(){	//2级下拉显示个数
		$(this).find('.nav_sec_list').each(function(index){
			if(big_width){
				var limit=3;
			}else{
				var limit=2;
			}
			if(index==limit) $(this).after('<div class="clear"></div>');
		});
	});
	/****** 导航显示 End ******/
	$('#header .search').click(function(){
		$('#header .search_form').toggle();	
	});
	$('#header .search_form i').click(function(){
		$('#header .search_form').hide();	
	});
	/****** 会员登录 Start ******/
	if($('.global_account_sec').size() || $('.global_login_sec').size()){
		if($('.global_account_sec').size()){	//已登录
			var account_name=$('.AccountButton_sec').text();
			$('.AccountButton_sec').html('<a rel="nofollow" href="/account/">'+account_name+'</a><i></i>');
			$('.global_account_sec').show();
		}else{
			$('.global_login_sec').show();
		}
	}
	/****** 会员登录 End ******/
});